;;; init.el --- Emacs Config -*- lexical-binding:
;; Author: Zonsopkomst
;; Type: Configuration
;; Created: 2023-09-17
;;
;;; Commentary:
;;
;; A copy and paste job with hopefully some thought, by Zonsopkomst
;;
;;; Code:



;; (message (system-name))


;; TODO
[[https://www.linuxjournal.com/content/org-mode-latex-exporter-latex-non-texers]]
[[https://config.daviwil.com/emacs]]
[[https://www.reddit.com/r/emacs/comments/6r32q4/orgmode_whats_your_html_export_look_like/]]
[[https://blog.aaronbieber.com/2016/09/24/an-agenda-for-life-with-org-mode.html][Agenda]]
[[https://orgmode.org/worg/org-tutorials/orgtutorial_dto.html][Agenda]]

"Straight - Package Manager"
"straight.el: next-generation, purely functional package manager for the Emacs hacker."
[[https://github.com/radian-software/straight.el]]
;; Disable package.el in favor of straight.el
(setq package-enable-at-startup nil)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Integration with use-package
(straight-use-package 'use-package)

;; Configure use-package to use straight.el by default
(use-package straight
  :custom
  (straight-use-package-by-default t))



"CUSTOM"


"transparent windows"
 ;;(set-frame-parameter (selected-frame) 'alpha '(<active> . <inactive>))
 ;;(set-frame-parameter (selected-frame) 'alpha <both>)
   ;; (set-frame-parameter (selected-frame) 'alpha '(95 . 50))
   ;; (add-to-list 'default-frame-alist '(alpha . (95 . 50)))

 (defun toggle-transparency ()
   (interactive)
   (let ((alpha (frame-parameter nil 'alpha)))
     (set-frame-parameter
      nil 'alpha
      (if (eql (cond ((numberp alpha) alpha)
                     ((numberp (cdr alpha)) (cdr alpha))
                     ;; Also handle undocumented (<active> <inactive>) form.
                     ((numberp (cadr alpha)) (cadr alpha)))
               100)
          '(85 . 50) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)



"Garbage Collection"
"The default is 800 kilobytes.  Measured in bytes."
(setq gc-cons-threshold (* 50 1000 1000))



"Profile emacs startup"
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s seconds with %d garbage collections."
                     (emacs-init-time "%.2f")
                     gcs-done)))



"Removes *scratch* from buffer after the mode has been set."
(defun remove-scratch-buffer ()
  (if (get-buffer "*scratch*")
    (kill-buffer "*scratch*")))
(add-hook 'after-change-major-mode-hook 'remove-scratch-buffer)



"No more typing the whole yes or no. Just y or n will do."
;;(fset 'yes-or-no-p 'y-or-n-p)
(setq use-short-answers t)



"Remove confirmation if a file or buffer does not exist when you use C-x C-f or C-x b"
(setq confirm-nonexistent-file-or-buffer nil)



"Stop emacs from closing when hitting ESC"
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)



"Add proper word wrapping"
(global-visual-line-mode t)



"Visual Appearance"
;; (menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(context-menu-mode 1)



"Column Numbers"
;; column-number-mode
(column-number-mode)


"Line Numbers"
(global-display-line-numbers-mode 1)
(setq display-line-numbers-type 'relative)



"Copy / Paste Defaults"
(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour
  ;;(cua-selection-mode t) ;; cua goodness without copy/paste etc.



"Delete Other Windows"
(add-hook 'emacs-startup-hook
          (lambda () (delete-other-windows)) t)



"CUSTOM-SET-VARIABLES"

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("f25f174e4e3dbccfcb468b8123454b3c61ba94a7ae0a870905141b050ad94b8f" default))
 '(package-selected-packages
   '(helm outline-magic neotree minimap nix-mode org-roam org-bullets json-mode smartparens prettier-js which-key magit dracula-theme use-package rainbow-delimiters paredit markdown-mode all-the-icons)))
(custom-set-faces)
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 

;; Default Locale Coding System

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)



;; Allow access from emacsclient
(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (server-start))))




"CENTRALIZE BACKUP FILES"

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5)    ; and how many of the old



"THEME"


;;(use-package doom-themes
;;  :config
;;  ;; Global settings (defaults)
;;  (setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
;;        doom-themes-enable-italic t) ; if nil, italics is universally disabled
;;  (load-theme 'doom-wilmersdorf t)
;;
;;  ;; Enable flashing mode-line on errors
;;  (doom-themes-visual-bell-config)
;;  ;; Enable custom neotree theme (all-the-icons must be installed!)
;;  (doom-themes-neotree-config)
;;  ;; or for treemacs users
;;  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
;;  (doom-themes-treemacs-config)
;;  ;; Corrects (and improves) org-mode's native fontification.
;;  (doom-themes-org-config))
;;  ;; Icons for treemacs
;;  (setq doom-themes-treemacs-theme "doom-colors")

"dracula-theme"
[[https://github.com/dracula/emacs]]
;;(use-package dracula-theme
;;  :config
;;  (load-theme 'dracula t))



"monokai theme"
[[https://github.com/oneKelvinSmith/monokai-emacs]]
(use-package monokai-theme
  :config
  (load-theme 'monokai t))



"font"
(set-face-attribute 'default nil :family "SpaceMono Nerd Font" :weight 'regular :height 120)
(set-face-attribute 'fixed-pitch nil :font "SpaceMono Nerd Font" :weight 'bold :height 190)
(set-face-attribute 'variable-pitch nil :font "SpaceMono Nerd Font" :weight 'bold :height 1.3)



"Add frame borders and window dividers"
(modify-all-frames-parameters
 '((right-divider-width . 10)
   (internal-border-width . 10)))
(dolist (face '(window-divider
                window-divider-first-pixel
                window-divider-last-pixel))
  (face-spec-reset-face face)
  (set-face-foreground face (face-attribute 'default :background)))
(set-face-background 'fringe (face-attribute 'default :background))



"LANGUAGES / MODES"

"json"
[[https://github.com/json-emacs/json-mode]]
(use-package json-mode)


"nix-mode"
"Nix mode for editing nix expressions"
[[https://github.com/NixOS/nix-mode]]
(use-package nix-mode
  :mode "\\.nix\\'")



"lsp-mode"
"lsp mode for expressions"
[[https://github.com/emacs-lsp/lsp-mode]]
(use-package lsp-mode
  :defer t
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (setq lsp-headerline-breadcrumb-enable nil))

(use-package dap-mode
  :after lsp-mode
  :ensure t
  :defer t)

(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t)
  (setq lsp-ui-sideline-delay 0.05))



"PACKAGES"
;; In Alphabetical Order



"ace-window"
[[https://github.com/abo-abo/ace-window]]
(use-package ace-window)
(global-set-key (kbd "M-o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
(defvar aw-dispatch-alist
  '((?x aw-delete-window "Delete Window"
    (?m aw-swap-window "Swap Windows")
    (?M aw-move-window "Move Window")
    (?c aw-copy-window "Copy Window")
    (?j aw-switch-buffer-in-window "Select Buffer")
    (?n aw-flip-window)
    (?u aw-switch-buffer-other-window "Switch Buffer Other Window")
    (?c aw-split-window-fair "Split Fair Window")
    (?v aw-split-window-vert "Split Vert Window")
    (?b aw-split-window-horz "Split Horz Window")
    (?o delete-other-windows "Delete Other Windows")
    (?? aw-show-dispatch-help)))
  "List of actions for `aw-dispatch-default'.")


"all-the-icons"
[[https://github.com/domtronn/all-the-icons.el]]
(use-package all-the-icons
  :if (display-graphic-p))



"avy"
"Jump to other windows"
[[https://github.com/abo-abo/avy]]
(use-package avy)
(global-set-key (kbd "C-:") 'avy-goto-char)
(avy-setup-default)
(global-set-key (kbd "C-c C-j") 'avy-resume)



"auto-package-update"
[[https://github.com/rranelli/auto-package-update.el]]
(use-package auto-package-update)
(auto-package-update-maybe)



"async"
"Emacs looks less often."
(use-package async
  :defer t
  :init
  (dired-async-mode 1))

(use-package savehist
  :defer 2
  :init (savehist-mode t))

(use-package repeat
  :defer 10
  :init
  (repeat-mode +1))



"beacon"
"highlight cursor"
[[https://github.com/Malabarba/beacon]]
(use-package beacon)
(beacon-mode 1)
(setq beacon-push-mark 35)
(setq beacon-color "#666600")



"centaur-tabs"
"Emacs tabs"
[[https://github.com/ema2159/centaur-tabs]]
(use-package centaur-tabs
  :if window-system
  :demand
  :init
  ;; Set the style to alternate with icons
  (setq centaur-tabs-style "alternate")
  (setq centaur-tabs-set-icons t)

  :config
  ;; Enable centaur-tabs
  (centaur-tabs-mode t)
  (centaur-tabs-headline-match)
  (setq centaur-tabs-set-bar 'over)
  (setq centaur-tabs-modified-marker "◌"))
(setq centaur-tabs-height 36
      centaur-tabs-set-icons t
      centaur-tabs-modified-marker "●"
      centaur-tabs-close-button "⊗"
      centaur-tabs-gray-out-icons 'buffer)
(setq x-underline-at-descent-line t)
(centaur-tabs-change-fonts "JetBrains Mono" 160)



"corfu"
"In-buffer Completion (with tab)"
[[https://github.com/minad/corfu]]
(use-package corfu
  ;; Optional customizations
  ;; :custom
  ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  ;; (corfu-auto t)                 ;; Enable auto completion
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since Dabbrev can be used globally (M-/).
  ;; See also `global-corfu-modes'.
  :init
  (global-corfu-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  (setq completion-cycle-threshold 3)

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete))



"dashboard"
"A dashboard for Emacs"
[[https://github.com/emacs-dashboard/emacs-dashboard]]
(use-package dashboard
  :config
  (dashboard-setup-startup-hook))
;; Set the title
(setq dashboard-banner-logo-title "Zonsemacs | C-x C-f Open a file | C-x k Kill Buffer | C-h f TERM  RET ")
;; Set the banner
;;(setq dashboard-startup-banner 'logo)
(setq dashboard-startup-banner
      (pcase (system-name)
        ("weda" "~/.emacs.d/00 assets/ac273182b3bbd00f9e72223861d3938b.png")
        ("tyr" "~/.emacs.d/00 assets/ac273182b3bbd00f9e72223861d3938b.png")
        ("DESKTOP-FIS3P5B" "c:/Users/Church/AppData/Roaming/.emacs.d/00 assets/ac273182b3bbd00f9e72223861d3938b.png")))  
;; Value can be
;; - nil to display no banner
;; - 'official which displays the official emacs logo
;; - 'logo which displays an alternative emacs logo
;; - 1, 2 or 3 which displays one of the text banners
;; - "path/to/your/image.gif", "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever gif/image/text you would prefer
;; - a cons of '("path/to/your/image.png" . "path/to/your/text.txt")

;; Content is not centered by default. To center, set
(setq dashboard-center-content t)

;; To disable shortcut "jump" indicators for each section, set
(setq dashboard-show-shortcuts nil)

(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)
                        (registers . 5)))

(setq dashboard-set-navigator t)
(setq dashboard-set-init-info t)

(setq dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name)
(setq dashboard-projects-switch-function 'projectile-persp-switch-project)

(add-to-list 'dashboard-items '(agenda) t)
(setq dashboard-week-agenda t)



"dired+"
[[https://www.emacswiki.org/emacs/DiredPlus]]
;; Open dired folders in same buffer
;; (put 'dired-find-alternate-file 'disabled nil)

;; (setq dired-guess-shell-alist-user `(("\\.png\\'" "gimp")
;;                                      ("\\.jpe?g\\'" "gimp")
;;                                      ("\\.mp4\\'" "vlc")))

(use-package dired+
;; nil
;;  :commands
;;  (dired dired-jump)
  :custom
  (dired-listing-switches
   "-goah --group-directories-first --time-style=long-iso")
  (dired-dwim-target t)
  (delete-by-moving-to-trash t))
  

;; Hide hidden files
(use-package dired-hide-dotfiles
  :hook
  (dired-mode . dired-hide-dotfiles-mode)
  :bind
  (:map dired-mode-map ("." . dired-hide-dotfiles-mode)))

;; Keep folders clean (create new directory when not yet existing)
;;(make-directory (expand-file-name "backups/" user-emacs-directory) t)
(setq-default backup-directory-alist
              `(("." . ,(expand-file-name "backups/" user-emacs-directory)))
              create-lockfiles nil)  ; No lock files

;;; Configure outline minor modes
;; Less crazy key bindings for outline-minor-mode
(setq outline-minor-mode-prefix "\C-c\C-o")
;; load outline-magic along with outline-minor-mode
(add-hook 'outline-minor-mode-hook 
      (lambda ()
        (require 'outline-magic)
        (define-key outline-minor-mode-map "\C-c\C-o\t" 'outline-cycle)
       (define-key outline-minor-mode-map (kbd "<backtab>") 'outline-cycle)))

;; revert dired and other buffers
(setq global-auto-revert-non-file-buffers t)


(use-package doom-modeline
  :init (doom-modeline-mode 1))
;; If non-nil, cause imenu to see `doom-modeline' declarations.
;; This is done by adjusting `lisp-imenu-generic-expression' to
;; include support for finding `doom-modeline-def-*' forms.
;; Must be set before loading doom-modeline.
(setq doom-modeline-support-imenu t)

;; How tall the mode-line should be. It's only respected in GUI.
;; If the actual char height is larger, it respects the actual height.
(setq doom-modeline-height 25)

;; How wide the mode-line bar should be. It's only respected in GUI.
(setq doom-modeline-bar-width 4)

;; Whether to use hud instead of default bar. It's only respected in GUI.
(setq doom-modeline-hud nil)

;; The limit of the window width.
;; If `window-width' is smaller than the limit, some information won't be
;; displayed. It can be an integer or a float number. `nil' means no limit."
(setq doom-modeline-window-width-limit 85)

;; How to detect the project root.
;; nil means to use `default-directory'.
;; The project management packages have some issues on detecting project root.
;; e.g. `projectile' doesn't handle symlink folders well, while `project' is unable
;; to hanle sub-projects.
;; You can specify one if you encounter the issue.
(setq doom-modeline-project-detection 'auto)

;; Determines the style used by `doom-modeline-buffer-file-name'.
;;
;; Given ~/Projects/FOSS/emacs/lisp/comint.el
;;   auto => emacs/l/comint.el (in a project) or comint.el
;;   truncate-upto-project => ~/P/F/emacs/lisp/comint.el
;;   truncate-from-project => ~/Projects/FOSS/emacs/l/comint.el
;;   truncate-with-project => emacs/l/comint.el
;;   truncate-except-project => ~/P/F/emacs/l/comint.el
;;   truncate-upto-root => ~/P/F/e/lisp/comint.el
;;   truncate-all => ~/P/F/e/l/comint.el
;;   truncate-nil => ~/Projects/FOSS/emacs/lisp/comint.el
;;   relative-from-project => emacs/lisp/comint.el
;;   relative-to-project => lisp/comint.el
;;   file-name => comint.el
;;   buffer-name => comint.el<2> (uniquify buffer name)
;;
;; If you are experiencing the laggy issue, especially while editing remote files
;; with tramp, please try `file-name' style.
;; Please refer to https://github.com/bbatsov/projectile/issues/657.
(setq doom-modeline-buffer-file-name-style 'auto)

;; Whether display icons in the mode-line.
;; While using the server mode in GUI, should set the value explicitly.
(setq doom-modeline-icon t)

;; Whether display the icon for `major-mode'. It respects `doom-modeline-icon'.
(setq doom-modeline-major-mode-icon t)

;; Whether display the colorful icon for `major-mode'.
;; It respects `nerd-icons-color-icons'.
(setq doom-modeline-major-mode-color-icon t)

;; Whether display the icon for the buffer state. It respects `doom-modeline-icon'.
(setq doom-modeline-buffer-state-icon t)

;; Whether display the modification icon for the buffer.
;; It respects `doom-modeline-icon' and `doom-modeline-buffer-state-icon'.
(setq doom-modeline-buffer-modification-icon t)

;; Whether display the time icon. It respects variable `doom-modeline-icon'.
(setq doom-modeline-time-icon t)

;; Whether to use unicode as a fallback (instead of ASCII) when not using icons.
(setq doom-modeline-unicode-fallback nil)

;; Whether display the buffer name.
(setq doom-modeline-buffer-name t)

;; Whether highlight the modified buffer name.
(setq doom-modeline-highlight-modified-buffer-name t)

;; When non-nil, mode line displays column numbers zero-based.
;; See `column-number-indicator-zero-based'.
(setq doom-modeline-column-zero-based t)

;; Specification of \"percentage offset\" of window through buffer.
;; See `mode-line-percent-position'.
(setq doom-modeline-percent-position '(-3 "%p"))

;; Format used to display line numbers in the mode line.
;; See `mode-line-position-line-format'.
(setq doom-modeline-position-line-format '("L%l"))

;; Format used to display column numbers in the mode line.
;; See `mode-line-position-column-format'.
(setq doom-modeline-position-column-format '("C%c"))

;; Format used to display combined line/column numbers in the mode line. See `mode-line-position-column-line-format'.
(setq doom-modeline-position-column-line-format '("%l:%c"))

;; Whether display the minor modes in the mode-line.
(setq doom-modeline-minor-modes nil)

;; If non-nil, a word count will be added to the selection-info modeline segment.
(setq doom-modeline-enable-word-count nil)

;; Major modes in which to display word count continuously.
;; Also applies to any derived modes. Respects `doom-modeline-enable-word-count'.
;; If it brings the sluggish issue, disable `doom-modeline-enable-word-count' or
;; remove the modes from `doom-modeline-continuous-word-count-modes'.
(setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))

;; Whether display the buffer encoding.
(setq doom-modeline-buffer-encoding t)

;; Whether display the indentation information.
(setq doom-modeline-indent-info nil)

;; Whether display the total line number。
(setq doom-modeline-total-line-number nil)

;; If non-nil, only display one number for checker information if applicable.
(setq doom-modeline-checker-simple-format t)

;; The maximum number displayed for notifications.
(setq doom-modeline-number-limit 99)

;; The maximum displayed length of the branch name of version control.
(setq doom-modeline-vcs-max-length 12)

;; Whether display the workspace name. Non-nil to display in the mode-line.
(setq doom-modeline-workspace-name t)

;; Whether display the perspective name. Non-nil to display in the mode-line.
(setq doom-modeline-persp-name t)

;; If non nil the default perspective name is displayed in the mode-line.
(setq doom-modeline-display-default-persp-name nil)

;; If non nil the perspective name is displayed alongside a folder icon.
(setq doom-modeline-persp-icon t)

;; Whether display the `lsp' state. Non-nil to display in the mode-line.
(setq doom-modeline-lsp t)

;; Whether display the GitHub notifications. It requires `ghub' package.
(setq doom-modeline-github nil)

;; The interval of checking GitHub.
(setq doom-modeline-github-interval (* 30 60))

;; Whether display the modal state.
;; Including `evil', `overwrite', `god', `ryo' and `xah-fly-keys', etc.
(setq doom-modeline-modal t)

;; Whether display the modal state icon.
;; Including `evil', `overwrite', `god', `ryo' and `xah-fly-keys', etc.
(setq doom-modeline-modal-icon t)

;; Whether display the modern icons for modals.
(setq doom-modeline-modal-modern-icon t)

;; When non-nil, always show the register name when recording an evil macro.
(setq doom-modeline-always-show-macro-register nil)

;; Whether display the mu4e notifications. It requires `mu4e-alert' package.
;;(setq doom-modeline-mu4e nil)
;; also enable the start of mu4e-alert
;;(mu4e-alert-enable-mode-line-display)

;; Whether display the gnus notifications.
(setq doom-modeline-gnus t)

;; Whether gnus should automatically be updated and how often (set to 0 or smaller than 0 to disable)
(setq doom-modeline-gnus-timer 2)

;; Wheter groups should be excludede when gnus automatically being updated.
(setq doom-modeline-gnus-excluded-groups '("dummy.group"))

;; Whether display the IRC notifications. It requires `circe' or `erc' package.
(setq doom-modeline-irc t)

;; Function to stylize the irc buffer names.
(setq doom-modeline-irc-stylize 'identity)

;; Whether display the battery status. It respects `display-battery-mode'.
(setq doom-modeline-battery t)

;; Whether display the time. It respects `display-time-mode'.
(setq doom-modeline-time t)

;; Whether display the misc segment on all mode lines.
;; If nil, display only if the mode line is active.
(setq doom-modeline-display-misc-in-all-mode-lines t)

;; Whether display the environment version.
(setq doom-modeline-env-version t)
;; Or for individual languages
(setq doom-modeline-env-enable-python t)
(setq doom-modeline-env-enable-ruby t)
(setq doom-modeline-env-enable-perl t)
(setq doom-modeline-env-enable-go t)
(setq doom-modeline-env-enable-elixir t)
(setq doom-modeline-env-enable-rust t)

;; Change the executables to use for the language version string
(setq doom-modeline-env-python-executable "python") ; or `python-shell-interpreter'
(setq doom-modeline-env-ruby-executable "ruby")
(setq doom-modeline-env-perl-executable "perl")
(setq doom-modeline-env-go-executable "go")
(setq doom-modeline-env-elixir-executable "iex")
(setq doom-modeline-env-rust-executable "rustc")

;; What to display as the version while a new one is being loaded
(setq doom-modeline-env-load-string "...")

;; By default, almost all segments are displayed only in the active window. To
;; display such segments in all windows, specify e.g.
;;(setq doom-modeline-always-visible-segments '(mu4e irc))

;; Hooks that run before/after the modeline version string is updated
(setq doom-modeline-before-update-env-hook nil)
(setq doom-modeline-after-update-env-hook nil)



"emojify"
[[https://github.com/iqbalansari/emacs-emojify]]
(use-package emojify
  :hook (after-init . global-emojify-mode))
(add-hook 'after-init-hook #'global-emojify-mode)



"expand region"
[[https://github.com/magnars/expand-region.el]]
(use-package expand-region)
  (require 'expand-region)
  (global-set-key (kbd "C-=") 'er/expand-region)



"flycheck"
"Syntax checking"
[[https://www.flycheck.org/en/latest/user/quickstart.html]]
(use-package flycheck
  :init (global-flycheck-mode))
  (add-hook 'after-init-hook #'global-flycheck-mode)


"format-all"
"Format code"
[[https://github.com/lassik/emacs-format-all-the-code]]
(use-package format-all)



"highlight todo"
[[https://github.com/tarsius/hl-todo]]
;;(use-package hl-todo)
;;(setq hl-todo-keyword-faces
  ;;    '(("TODO"   . "#FF0000")
    ;;    ("WAIT"  . "#FF0000")
      ;;  ("HOLD"  . "#A020F0")
       ;; ("SCHD"   . "#1E90FF")))



"highligh indent guide"
[[https://github.com/DarthFennec/highlight-indent-guides]]
(use-package highlight-indent-guides
  :defer t
  :hook (prog-mode . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-character ?\|)
  (setq highlight-indent-guides-responsive 'top))



"ialign"
"Emacs package that provides visual version of align-regexp command."
[[https://github.com/mkcms/interactive-align]]
(use-package ialign)
(require 'ialign)
(global-set-key (kbd "C-x l") #'ialign)



"iedit"
"Edit multiple occurrences in the same way simultaneously"
[[https://github.com/victorhge/iedit]]
(use-package iedit)
(require 'iedit)
(defun iedit-dwim (arg)
  "Starts iedit but uses \\[narrow-to-defun] to limit its scope."
  (interactive "P")
  (if arg
      (iedit-mode)
    (save-excursion
      (save-restriction
        (widen)
        ;; this function determines the scope of `iedit-start'.
        (if iedit-mode
            (iedit-done)
          ;; `current-word' can of course be replaced by other
          ;; functions.
          (narrow-to-defun)
          (iedit-start (current-word) (point-min) (point-max)))))))
(global-set-key (kbd "C-;") 'iedit-dwim)


"indent guide"
[[https://github.com/zk-phi/indent-guide]]
(use-package indent-guide)
  (require 'indent-guide)
  (indent-guide-global-mode)
  (set-face-background 'indent-guide-face "dimgray")

  ;; You may also change the character for guides.
  (setq indent-guide-char ":")



"magit"
[[https://github.com/magit/magit]]
(use-package magit)
;; Install package `delta`
;; Windows: `choco install delta`
;; Nix: `delta`
(use-package magit-delta
  :hook (magit-mode . magit-delta-mode))
(use-package git-timemachine)
(use-package magit-section)



"marginalia"
(use-package marginalia
  :config
  (marginalia-mode))

  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
 ;; :bind (:map minibuffer-local-map
 ;;             ("M-A" . marginalia-cycle))

(use-package embark
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
  ;; strategy, if you want to see the documentation from multiple providers.
  (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)
  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
           '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
             nil
             (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))



"minimap"
[[https://github.com/dengste/minimap]]
(use-package minimap)
(minimap-create)
(setq minimap-window-location 'right)
(custom-set-faces
  '(minimap-active-region-background
    ((((background dark)) (:background "#551a8b"))
     (t (:background "#D3D3D3222222")))
    "Face for the active region in the minimap.
By default, this is only a different background color."
    :group 'minimap))



"move-text"
[[https://github.com/emacsfodder/move-texts]]
(use-package move-text)
(move-text-default-bindings)
(defun indent-region-advice (&rest ignored)
  (let ((deactivate deactivate-mark))
    (if (region-active-p)
        (indent-region (region-beginning) (region-end))
      (indent-region (line-beginning-position) (line-end-position)))
    (setq deactivate-mark deactivate)))

(advice-add 'move-text-up :after 'indent-region-advice)
(advice-add 'move-text-down :after 'indent-region-advice)



"multiple-cursors"
[[https://github.com/magnars/multiple-cursors.el]]
(use-package multiple-cursors)
(require 'multiple-cursors)
(global-set-key (kbd "C-|") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
(define-key mc/keymap (kbd "<return>") nil)



"mw-thesaurus"
[[https://github.com/agzam/mw-thesaurus.el]]
(use-package mw-thesaurus
  :defer t
  :commands mw-thesaurus-lookup-dwim
  :hook (mw-thesaurus-mode . variable-pitch-mode)
  :config
  (map! :map mw-thesaurus-mode-map [remap evil-record-macro] #'mw-thesaurus--quit)

  ;; window on the right side
  (add-to-list
   'display-buffer-alist
   `(,mw-thesaurus-buffer-name
     (display-buffer-reuse-window
      display-buffer-in-direction)
     (direction . right)
     (window . root)
     (window-width . 0.3))))


"olivetti"
"Distraction-free writing"
  (defun ews-distraction-free ()
    "Distraction-free writing environment using Olivetti package."
    (interactive)
    (if (equal olivetti-mode nil)
        (progn
          (window-configuration-to-register 1)
          (delete-other-windows)
          (text-scale-set 2)
          (olivetti-mode t))
      (progn
        (if (eq (length (window-list)) 1)
            (jump-to-register 1))
        (olivetti-mode 0)
        (text-scale-set 0))))

  (use-package olivetti
    :demand t
    :bind
    (("<f9>" . ews-distraction-free)))



"orderless"
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))



"org-mode"
"Org Mode is an authoring tool and a TODO lists manager for GNU Emacs. It relies on a lightweight plain-text markup language used in files with the ‘.org’ extension."
(use-package org :demand t) 


"org-agenda"
;;(setq org-agenda-files 
;;  (cond ((string-equal (system-name) "weda")
;;	 (setq org-roam-directory "~/Syncthing/org-roam/stpaul's/00 org/todo.org"))
;;       ((string-equal (system-name) "tyr")
;;	 (setq org-roam-directory "~/Syncthing/org-roam/stpaul's/00 org/todo.org"))
;;        ((string-equal (system-name) "DESKTOP-FIS3P5B")
;;	 (setq org-roam-directory "c:/Users/Church/Documents/Syncthing/org-roam/stpaul's/00 org/stpaul's.org"))))
;;


;; set the default export scope to subtree
;;(setf org-export-initial-scope 'subtree)



(setq org-html-head "<meta http-equiv='X-UA-Compatible' content='IE=edge'><meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'><style>html{touch-action:manipulation;-webkit-text-size-adjust:100%}body{padding:0;margin:0;background:#f2f6fa;color:#3c495a;font-weight:normal;font-size:15px;font-family:'San Francisco','Roboto','Arial',sans-serif}h2,h3,h4,h5,h6{font-family:'Trebuchet MS',Verdana,sans-serif;color:#586b82;padding:0;margin:20px 0 10px 0;font-size:1.1em}h2{margin:30px 0 10px 0;font-size:1.2em}a{color:#3fa7ba;text-decoration:none}p{margin:6px 0;text-align:justify}ul,ol{margin:0;text-align:justify}ul>li>code{color:#586b82}pre{white-space:pre-wrap}#content{width:96%;max-width:1000px;margin:2% auto 6% auto;background:white;border-radius:2px;border-right:1px solid #e2e9f0;border-bottom:2px solid #e2e9f0;padding:0 115px 150px 115px;box-sizing:border-box}#postamble{display:none}h1.title{background-color:#343C44;color:#fff;margin:0 -115px;padding:60px 0;font-weight:normal;font-size:2em;border-top-left-radius:2px;border-top-right-radius:2px}@media (max-width: 1050px){#content{padding:0 70px 100px 70px}h1.title{margin:0 -70px}}@media (max-width: 800px){#content{width:100%;margin-top:0;margin-bottom:0;padding:0 4% 60px 4%}h1.title{margin:0 -5%;padding:40px 5%}}pre,.verse{box-shadow:none;background-color:#f9fbfd;border:1px solid #e2e9f0;color:#586b82;padding:10px;font-family:monospace;overflow:auto;margin:6px 0}#table-of-contents{margin-bottom:50px;margin-top:50px}#table-of-contents h2{margin-bottom:5px}#text-table-of-contents ul{padding-left:15px}#text-table-of-contents>ul{padding-left:0}#text-table-of-contents li{list-style-type:none}#text-table-of-contents a{color:#7c8ca1;font-size:0.95em;text-decoration:none}table{border-color:#586b82;font-size:0.95em}table thead{color:#586b82}table tbody tr:nth-child(even){background:#f9f9f9}table tbody tr:hover{background:#586b82!important;color:white}table .left{text-align:left}table .right{text-align:right}.todo{font-family:inherit;color:inherit}.done{color:inherit}.tag{background:initial}.tag>span{background-color:#eee;font-family:monospace;padding-left:7px;padding-right:7px;border-radius:2px;float:right;margin-left:5px}#text-table-of-contents .tag>span{float:none;margin-left:0}.timestamp{color:#7c8ca1}@media print{@page{margin-bottom:3cm;margin-top:3cm;margin-left:2cm;margin-right:2cm;font-size:10px}#content{border:none}}</style>")



"Export to Markdown"
(eval-after-load "org"
  '(require 'ox-md nil t))



(setq org-todo-keywords
      '((sequence "TODO(t)"  "HOLD(h)" "SCHD(s)" "|" "DONE(d)" "FAIL(f)")))



  (use-package org-appear
    :hook
    (org-mode . org-appear-mode))
    (org-toggle-inline-images 1)


;;(use-package org-bullets)
;;(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

"org-modern"
[[https://github.com/minad/org-modern]]
  (use-package org-modern
    :hook
    (org-mode . global-org-modern-mode)
    :custom
    (org-modern-keyword nil)
    (org-modern-checkbox nil)
    (org-modern-table nil))

 

(require 'org-mouse)

(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)





  (add-hook 'org-mode-hook 'visual-line-mode)



"Automatically mark parent as done when subtasks are completed"
(defun org-summary-todo (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))

(add-hook 'org-after-todo-statistics-hook #'org-summary-todo)



(setq org-log-done 'time)
(setq org-log-done 'note)



"Improve org mode looks"
[[https://lucidmanager.org/productivity/ricing-org-mode/]]
    (setq-default org-startup-indented t
                  org-pretty-entities t
                  org-use-sub-superscripts "{}"
                  org-hide-emphasis-markers t
                  org-startup-with-inline-images t
                  org-tags-column 0
		  org-catch-invisible-edits 'show-and-error
                  org-special-ctrl-a/e t
                  org-insert-heading-respect-content t
		  org-image-actual-width '(300))

  ;; Increase line spacing
  (setq-default line-spacing 2)



;;"LaTeX previews"
;;  (use-package org-fragtog
;;    :after org
;;    :custom
;;    (org-startup-with-latex-preview t)
;;    :hook
;;    (org-mode . org-fragtog-mode)
;;    :custom
;;    (org-format-latex-options
;;     (plist-put org-format-latex-options :scale 2)
;;     (plist-put org-format-latex-options :foreground 'auto)
;;     (plist-put org-format-latex-options :background 'auto)))


"org-inlinetask"
"Including code for a better return"
(require 'org-inlinetask)

;;(defun scimax/org-return (&optional ignore)
;;  "Add new list item, heading or table row with RET.
;;A double return on an empty element deletes it.
;;Use a prefix arg to get regular RET. "
;;  (interactive "P")
;;  (if ignore
;;      (org-return)
;;    (cond
;;
;;     ((eq 'line-break (car (org-element-context)))
;;      (org-return-indent))
;;
;;     ;; Open links like usual, unless point is at the end of a line.
;;     ;; and if at beginning of line, just press enter.
;;     ((or (and (eq 'link (car (org-element-context))) (not (eolp)))
;;          (bolp))
;;      (org-return))
;;
;;     ;; It doesn't make sense to add headings in inline tasks. Thanks Anders
;;     ;; Johansson!
;;     ((org-inlinetask-in-task-p)
;;      (org-return))
;;
;;     ;; checkboxes too
;;     ((org-at-item-checkbox-p)
;;      (org-insert-todo-heading nil))
;;
;;     ;; lists end with two blank lines, so we need to make sure we are also not
;;     ;; at the beginning of a line to avoid a loop where a new entry gets
;;     ;; created with only one blank line.
;;     ((org-in-item-p)
;;      (if (save-excursion (beginning-of-line) (org-element-property :contents-begin (org-element-context)))
;;          (org-insert-heading)
;;        (beginning-of-line)
;;        (delete-region (line-beginning-position) (line-end-position))
;;        (org-return)))
;;
;;     ;; org-heading
;;     ((org-at-heading-p)
;;      (if (not (string= "" (org-element-property :title (org-element-context))))
;;          (progn (org-end-of-meta-data)
;;                 (org-insert-heading-respect-content)
;;                 (outline-show-entry))
;;        (beginning-of-line)
;;        (setf (buffer-substring
;;               (line-beginning-position) (line-end-position)) "")))
;;
;;     ;; tables
;;     ((org-at-table-p)
;;      (if (-any?
;;           (lambda (x) (not (string= "" x)))
;;           (nth
;;            (- (org-table-current-dline) 1)
;;            (org-table-to-lisp)))
;;          (org-return)
;;        ;; empty row
;;        (beginning-of-line)
;;        (setf (buffer-substring
;;               (line-beginning-position) (line-end-position)) "")
;;        (org-return)))
;;
;;     ;; fall-through case
;;     (t
;;      (org-return)))))
;;
;;
;;(define-key org-mode-map (kbd "RET")
;;  'scimax/org-return)
;;


"org-roam"
"Org-roam is a plain-text knowledge management system. It brings some of Roam's more powerful features into the Org-mode ecosystem."
[[https://github.com/org-roam/org-roam]]
(use-package org-roam
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :custom 
  (setq org-roam-complete-everywhere t)

  :config
;; Change path for PC

;;  (setq org-roam-directory
;;      (pcase (system-name)
;;        ("weda" "~/Syncthing/org-roam/")
;;        ("tyr" "~/Syncthing/org-roam/")
;;        ("DESKTOP-FIS3P5B" "c:/Users/Church/Documents/Syncthing/org-roam/")))  

;;  (setq org-roam-db-location
;;      (pcase (system-name)
;;        ("weda" "~/Syncthing/org-roam/org-roam.db")
;;        ("tyr" "~/Syncthing/org-roam/org-roam.db")
;;        ("DESKTOP-FIS3P5B" "c:/Users/Church/Documents/Syncthing/org-roam/org-roam.db"))))  

  (cond ((string-equal (system-name) "weda")
	 (setq org-roam-directory "~/Syncthing/org-roam/"))
        ((string-equal (system-name) "tyr")
	 (setq org-roam-directory "~/Syncthing/org-roam/"))
        ((string-equal (system-name) "DESKTOP-FIS3P5B")
	 (setq org-roam-directory "c:/Users/Church/Documents/Syncthing/org-roam/"))) 

  (cond ((string-equal (system-name) "weda")
	 (setq org-roam-db-location "~/Syncthing/org-roam/org-roam.db"))
        ((string-equal (system-name) "tyr")
	 (setq org-roam-db-location "~/Syncthing/org-roam/org-roam.db"))
        ((string-equal (system-name) "DESKTOP-FIS3P5B")
	 (setq org-roam-db-location "c:/Users/Church/Documents/Syncthing/org-roam/org-roam.db"))) 

  (setq org-roam-capture-templates 
      '( 
          ("d" "default" plain "%?" 
          :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
	  "#+title: ${title}\n")
	  :unnarrowed t)
	  
          ("b" "baptism" plain
	  "\n* Source\n\nAuthor: %^{Author}\nTitle: ${title}\nDate: %^{%T}\n\n* Summary\n\n%?"
	  :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
	  :unnarrowed t)

          ("e" "expense report" plain
	   "\n#+Author: Eric Wagner\n#+tags:\n#+OPTIONS: num:nil author:nil toc:nil\n* Expense Report\n#+ATTR_HTML: :border 2 :rules all :frame border\n|Field|Value|\n|---|---|\n|**Request for Check**|AMOUNT|\n|**Date**|%t|\n|**Reference**|${title}|\n|**Payable to**|PAYABLE_TO|\n|**Street Address**|n/a|\n|**Vendor(s)**|VENDOR|\n|**Reason**|REASON|\n|**Requested by**|NAME|\n|**Account Number**|ACT|" 
          :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
	  "#+title: ${title}\n")
	  :unnarrowed t)

          ("f" "flower order" plain
	   "\n#+Author: Eric Wagner\n#+tags:\n#+OPTIONS: num:nil author:nil toc:nil\n* Flower Order\n#+ATTR_HTML: :border 2 :rules all :frame border\n|Field|Value|\n|---|---|\n|**Total Due**|AMOUNT|\n|**Date Entered**|%t|\n|**Date Scheduled**|xxxxxx\n|**Reference**|${title}|\n|**Type**|FLOWER_TYPE|\n|**Bulletin Message**|The TYPE are given by NAME |\n|**Arrangment Instructions**|INSTRUCTIONS|\n|**Pickup Time**|TIME|\n|**Requested by**|NAME|\n|\n * [ ] Add to Prayer Chain?" 
          :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
	  "#+title: ${title}\n")
	  :unnarrowed t)
	  
       ) 
  )

  (setq org-html-validation-link nil)

  (setq org-roam-node-display-template
        (concat "${title:*} "
                (propertize "${tags:10}" 'face 'org-tag)))
  
  (org-roam-setup))

;;"Set Backlinks"
;;[[https://www.orgroam.com/manual.html#Configuring-what-is-displayed-in-the-buffer]]
;;(setq org-roam-mode-sections
;;      '((org-roam-backlinks-section :unique t)
;;        org-roam-reflinks-section))

;;  (org-roam-autosync-mode)



(use-package org-roam-ui
;;  :straight
;;    (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
    :after org-roam
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
;;    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start f
    )
)



"page-break-lines"
"Display form feed characters as tidy horizontal lines"
[[https://github.com/purcell/page-break-lines]]
(use-package page-break-lines)
  (page-break-lines-mode)



"popper"
"Designate any buffer to “popup” status, and it will stay out of your way"
[[https://github.com/karthink/popper]]
(use-package popper
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints



"prettier-js"
"Format the current buffer using prettier"
[[https://github.com/prettier/prettier-emacs]]
(use-package prettier-js
  :config
  (setq prettier-js-args '(
                           "--trailing-comma" "es5"
                           "--single-quote" "true"
                           "--print-width" "120"
                           "--tab-width" "4"
                           "--use-tabs" "false"
                           "--jsx-bracket-same-line" "false"
                           "--stylelint-integration" "true")))



"projectile"
"Project tool (find / switch / etc)"
[[https://github.com/bbatsov/projectile]]
(use-package projectile
  :defer t
  :config
  ;; Enable Projectile globally
  (setq projectile-completion-system 'ido)
  (setq ido-enable-flex-matching t)
  (projectile-mode 1))



"rainbow-delimiters"
"Rainbow parentheses"
[[https://github.com/Fanael/rainbow-delimiters]]
(use-package rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)



"restart-emacs"
[[https://github.com/iqbalansari/restart-emacs]]
(use-package restart-emacs)


"smartparens"
"Smart parenthesis"
[[https://github.com/Fuco1/smartparens]]
(use-package smartparens
  :init
  (smartparens-global-mode))



"smex"
"M-x enhancement"
[[https://github.com/nonsequitur/smex]]
(use-package smex
  :init (smex-initialize)
  :bind
  ("M-x" . smex))



"spell-fu"
"Highlight mispelled words"
[[https://github.com/emacsmirror/spell-fu]]
(use-package spell-fu)

;; (add-hook 'org-mode-hook
;;   (lambda ()
;;     (setq spell-fu-faces-exclude
;;      '(org-block-begin-line
;;        org-block-end-line
;;        org-code
;;        org-date
;;        org-drawer org-document-info-keyword
;;        org-ellipsis
;;        org-link
;;        org-meta-line
;;        org-properties
;;        org-properties-value
;;        org-special-keyword
;;        org-src
;;        org-tag
;;        org-verbatim))
;;     (spell-fu-mode)))

;;(add-hook 'emacs-lisp-mode-hook
;;  (lambda ()
;;    (spell-fu-mode)))



"svg-tag-mode"
"TODO rendorer for tags for certain keywords"
(use-package svg-tag-mode)
(setq svg-tag-tags
      '((":TODO:" . ((lambda (tag) (svg-tag-make "TODO"))))))
  (defun org-agenda-show-svg ()
    (let* ((case-fold-search nil)
           (keywords (mapcar #'svg-tag--build-keywords svg-tag--active-tags))
           (keyword (car keywords)))
      (while keyword
        (save-excursion
          (while (re-search-forward (nth 0 keyword) nil t)
            (overlay-put (make-overlay
                          (match-beginning 0) (match-end 0))
                         'display  (nth 3 (eval (nth 2 keyword)))) ))
        (pop keywords)
        (setq keyword (car keywords)))))
  (add-hook 'org-agenda-finalize-hook #'org-agenda-show-svg)



"swiper"
"Minibuffer completion tool"
[[https://github.com/abo-abo/swiper]]
(use-package swiper
  :bind (("M-s" . counsel-grep-or-swiper)))



"treemacs"
[[https://github.com/Alexander-Miller/treemacs]]
(use-package treemacs
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                  (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-display-current-project-exclusively  t ;;
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                2000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-header-scroll-indicators        '(nil . "^^^^^^")
          treemacs-hide-dot-git-directory          t
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-project-follow-mode             t ;; 
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-project-follow-into-home        nil
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    ;;(when treemacs-python-executable
    ;;  (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(with-eval-after-load 'treemacs
  (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action))

(add-hook 'emacs-startup-hook 'treemacs)

(use-package treemacs-projectile
  :after (treemacs projectile))

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once))

(use-package treemacs-magit
  :after (treemacs magit))

(use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs persp-mode) ;;or perspective vs. persp-mode
  :config (treemacs-set-scope-type 'Perspectives))

(use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
  :after (treemacs)
  :config (treemacs-set-scope-type 'Tabs));;



"undo-fu-session"
"Save & recover undo steps between Emacs sessions."
[[https://github.com/emacsmirror/undo-fu-session]]
(use-package undo-fu-session ; Persistant undo history
  :ensure t
  :demand t
  :bind (("C-x u"   . undo-only)
         ("C-/" . undo-only)
         ("C-?" . undo-redo)
         ("C-z"     . undo-only)
         ("C-S-z"   . undo-redo))
  :config (global-undo-fu-session-mode))



"vertico"
"Vertical completion UI"
[[https://github.com/minad/vertico]]
(use-package vertico
  :init
  (vertico-mode))

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  ;; (setq vertico-cycle t)

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))



"vimish-fold"
"VIM like folding"
[[https://github.com/matsievskiysv/vimish-fold]]
;;(use-package vimish-fold)
;;  (require 'vimish-fold)
;;   (vimish-fold-global-mode 1)



"which-key"
"Displays keybindings for incomplete command"
[[https://github.com/justbur/emacs-which-key]]
(use-package which-key
  :config
  (which-key-mode))



"xeft"
"Note searching interface"
"https://github.com/casouri/xeft"
(use-package xeft)



"yafolding"
"Folding code blocks based on indentation"
[[https://github.com/emacsorphanage/yafolding]]
(use-package yafolding)

(defvar yafolding-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<C-S-return>") #'yafolding-hide-parent-element)
    (define-key map (kbd "<C-M-return>") #'yafolding-toggle-all)
    (define-key map (kbd "<C-return>") #'yafolding-toggle-element)
    map))
  
(global-set-key (kbd "s-d y") 'yafolding-discover)
  
(add-hook 'prog-mode-hook
          (lambda () (yafolding-mode)))
  
(require 'yafolding)
(define-key yafolding-mode-map (kbd "<C-S-return>") nil)
(define-key yafolding-mode-map (kbd "<C-M-return>") nil)
(define-key yafolding-mode-map (kbd "<C-return>") nil)
(define-key yafolding-mode-map (kbd "C-c <C-M-return>") 'yafolding-toggle-all)
(define-key yafolding-mode-map (kbd "C-c <C-S-return>") 'yafolding-hide-parent-element)
(define-key yafolding-mode-map (kbd "C-c <C-return>") 'yafolding-toggle-element)



"yasnippet"
"Yet another snippet extension for Emacs"
"Creating the snippet
To create the snippet file execute the command `yas-new-snippet` (C-c & C-n), yasnippets internally expands the snippet-writing snippet.
A new buffer will open with the base model to define the new snippet in the snippet-mode major mode."
[[https://github.com/joaotavora/yasnippet]]
(use-package yasnippet
  :hook ((text-mode
          prog-mode
          conf-mode
          snippet-mode) . yas-minor-mode-on)
  :init
  ;;(setq yas-snippet-dir "~/.emacs.d/snippets")
    (setq yas-snippet-dir "~/Documents/Syncthing/org-roam/stpaul's/snippets"))



;;; init.el ends here
