;; Author: Zonsopkomst
;; Type: Configuration
;; Created: 2023-09-17


;; STARTUP

;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s seconds with %d garbage collections."
                     (emacs-init-time "%.2f")
                     gcs-done)))


;;enable STRAIGHT - Purley Functional Package Manager;;

;; Disable package.el in favor of straight.el
(setq package-enable-at-startup nil)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Integration with use-package
(straight-use-package 'use-package)

;; Configure use-package to use straight.el by default
(use-package straight
  :custom
  (straight-use-package-by-default t))



;; CUSTOM

;; Makes *scratch* empty.
;;;(setq initial-scratch-message "")

;; Removes *scratch* from buffer after the mode has been set.
;;;(defun remove-scratch-buffer ()
;;;  (if (get-buffer "*scratch*")
  ;;;    (kill-buffer "*scratch*")))
;;;(add-hook 'after-change-major-mode-hook 'remove-scratch-buffer)

;;(setq initial-scratch-message nil)
;;;(setq inhibit-startup-screen t)

;; start the initial frame maximized
;;;(add-to-list 'initial-frame-alist '(fullscreen . maximized))

;; start every frame maximized
;;;(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; No more typing the whole yes or no. Just y or n will do.
(fset 'yes-or-no-p 'y-or-n-p)

;; Revert buffers when the underlying file has changed
;;;(global-auto-revert-mode 1)

;; I mindlessly press ESC, so stop me from wreaking havoc
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; Scrolling behavior
;;;(setq redisplay-dont-pause t
;;;      scroll-margin 1
;;;      scroll-step 1
;;;      scroll-conservatively 10000
;;;      scroll-preserve-screen-position 1)

;;;(setq mouse-wheel-follow-mouse 't)
;;;(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))

;; Prefere single frames
;;;(setq ediff-window-setup-function 'ediff-setup-windows-plain)

;; use allout minor mode to have outlining everywhere.
;;;(allout-mode)

;; Add proper word wrapping
;;;(global-visual-line-mode t)

;; (menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(context-menu-mode 1)

;; column-number-mode
(column-number-mode)

;; Enable line numbers for some modes
(dolist (mode '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

;; Override some modes which derive from the above
;;(dolist (mode '(org-mode-hook))
;;  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(eval-after-load "linum"
  '(set-face-attribute 'linum nil :height 90))

(add-hook 'emacs-startup-hook
          (lambda () (delete-other-windows)) t)



;; CUSTOM-SET-VARIABLES

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("f25f174e4e3dbccfcb468b8123454b3c61ba94a7ae0a870905141b050ad94b8f" default))
 '(package-selected-packages
   '(helm outline-magic neotree minimap nix-mode org-roam org-bullets json-mode smartparens prettier-js which-key magit dracula-theme use-package rainbow-delimiters paredit markdown-mode all-the-icons)))
(custom-set-faces)
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 

(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)


;; Allow access from emacsclient
(add-hook 'after-init-hook
          (lambda ()
            (require 'server)
            (unless (server-running-p)
              (server-start))))



;; CENTRALIZE BACKUP FILES

(setq backup-directory-alist '(("." . "~/.emacs.d/backup"))
  backup-by-copying t    ; Don't delink hardlinks
  version-control t      ; Use version numbers on backups
  delete-old-versions t  ; Automatically delete excess backups
  kept-new-versions 20   ; how many of the newest versions to keep
  kept-old-versions 5)    ; and how many of the old


;; cua-mode
(cua-mode t)
(setq cua-auto-tabify-rectangles nil) ;; Don't tabify after rectangle commands
(transient-mark-mode 1) ;; No region when it is not highlighted
(setq cua-keep-region-after-copy t) ;; Standard Windows behaviour
  ;;(cua-selection-mode t) ;; cua goodness without copy/paste etc.


;; THEME

;;(use-package doom-themes
;;  :config
  ;; Global settings (defaults)
  ;;(setq doom-themes-enable-bold t    ; if nil, bold is universally disabled
  ;;      doom-themes-enable-italic t) ; if nil, italics is universally disabled
;;  (load-theme 'doom-wilmersdorf t)

  ;; Enable flashing mode-line on errors
;;  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
;;  (doom-themes-neotree-config)
  ;; or for treemacs users
;;  (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
;;  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
;;  (doom-themes-org-config))


(use-package dracula-theme
  :config
    (load-theme 'dracula t))

;; set font JetBrains Mono  11.5 pt
(set-face-attribute 'default nil
                    :family "JetBrains Mono"
                    :height 115)

;;(use-package nerd-icons
  ;; :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  ;; (nerd-icons-font-family "JetBrains Mono")
;;  )



;; PACKAGES;;
;; In Alphabetical Order

;; ace-window
(use-package ace-window)
(global-set-key (kbd "M-o") 'ace-window)
(setq aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
(defvar aw-dispatch-alist
  '((?x aw-delete-window "Delete Window"
                                                                                                	(?m aw-swap-window "Swap Windows")
                                                                                                	(?M aw-move-window "Move Window")
                                                                                                	(?c aw-copy-window "Copy Window")
                                                                                                	(?j aw-switch-buffer-in-window "Select Buffer")
                                                                                                	(?n aw-flip-window)
                                                                                                	(?u aw-switch-buffer-other-window "Switch Buffer Other Window")
                                                                                                	(?c aw-split-window-fair "Split Fair Window")
                                                                                                	(?v aw-split-window-vert "Split Vert Window")
                                                                                                	(?b aw-split-window-horz "Split Horz Window")
                                                                                                	(?o delete-other-windows "Delete Other Windows")
                                                                                                	(?? aw-show-dispatch-help)))
  "List of actions for `aw-dispatch-default'.")

(use-package all-the-icons
  :if (display-graphic-p))


(use-package avy)
(global-set-key (kbd "C-:") 'avy-goto-char)
(avy-setup-default)
(global-set-key (kbd "C-c C-j") 'avy-resume)

(use-package auto-package-update)
(auto-package-update-maybe)

(use-package beacon)
(beacon-mode 1)
(setq beacon-push-mark 35)
(setq beacon-color "#666600")


(use-package calendar)


;; centaur-tabs
(use-package centaur-tabs
  :if window-system
  :demand
  :init
  ;; Set the style to alternate with icons
  (setq centaur-tabs-style "alternate")
  (setq centaur-tabs-set-icons t)

  :config
  ;; Enable centaur-tabs
  (centaur-tabs-mode t)
  (centaur-tabs-headline-match)
  (setq centaur-tabs-set-bar 'over)
  (setq centaur-tabs-modified-marker "◌"))
(setq centaur-tabs-height 36
      centaur-tabs-set-icons t
      centaur-tabs-modified-marker "●"
      centaur-tabs-close-button "⊗"
      centaur-tabs-gray-out-icons 'buffer)
(setq x-underline-at-descent-line t)
(centaur-tabs-change-fonts "JetBrains Mono" 160)


(use-package clippy)


(use-package consult)

(require 'consult)
;;  (:global "C-s" consult-line
;;               "C-M-l" consult-imenu
;;               "C-M-j" persp-switch-to-buffer*)

;;  (:with-map minibuffer-local-map
;;    (:bind "C-r" consult-history))

(defun dw/get-project-root ()
  (when (fboundp 'projectile-project-root)
    (projectile-project-root)))

;;  (:option consult-project-root-function #'dw/get-project-root
;;           completion-in-region-function #'consult-completion-in-region))



;; counsel
(use-package counsel
  :bind (("M-x" . counsel-M-x)))

(use-package prescient)
(use-package ivy-prescient
  :config
  (ivy-prescient-mode t))


;; corfu;;
(use-package corfu
  ;; Optional customizations
  ;; :custom
  ;; (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  ;; (corfu-auto t)                 ;; Enable auto completion
  ;; (corfu-separator ?\s)          ;; Orderless field separator
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  ;; (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since Dabbrev can be used globally (M-/).
  ;; See also `global-corfu-modes'.
  :init
  (global-corfu-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; TAB cycle if there are only few candidates
  (setq completion-cycle-threshold 3)

  ;; Emacs 28: Hide commands in M-x which do not apply to the current mode.
  ;; Corfu commands are hidden, since they are not supposed to be used via M-x.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (setq tab-always-indent 'complete))


(use-package crux)

(use-package default-text-scale)


;; dashboard
(use-package dashboard
  :config
  (dashboard-setup-startup-hook))
;; Set the title
(setq dashboard-banner-logo-title "Emacs Dashboard")
;; Set the banner
(setq dashboard-startup-banner 'logo)
;; Value can be
;; - nil to display no banner
;; - 'official which displays the official emacs logo
;; - 'logo which displays an alternative emacs logo
;; - 1, 2 or 3 which displays one of the text banners
;; - "path/to/your/image.gif", "path/to/your/image.png" or "path/to/your/text.txt" which displays whatever gif/image/text you would prefer
;; - a cons of '("path/to/your/image.png" . "path/to/your/text.txt")

;; Content is not centered by default. To center, set
(setq dashboard-center-content t)

;; To disable shortcut "jump" indicators for each section, set
(setq dashboard-show-shortcuts nil)

(setq dashboard-items '((recents  . 5)
                        (bookmarks . 5)
                        (projects . 5)
                        (agenda . 5)
                        (registers . 5)))

(setq dashboard-set-navigator t)
(setq dashboard-set-init-info t)

(setq dashboard-projects-switch-function 'counsel-projectile-switch-project-by-name)
(setq dashboard-projects-switch-function 'projectile-persp-switch-project)

(add-to-list 'dashboard-items '(agenda) t)
(setq dashboard-week-agenda t)



;; denote
(use-package denote)
(require 'denote)

;; Remember to check the doc strings of those variables.
(setq denote-directory (expand-file-name "~/Documents/notes/"))
(setq denote-known-keywords '("emacs" "philosophy" "politics" "economics"))
(setq denote-infer-keywords t)
(setq denote-sort-keywords t)
(setq denote-file-type nil) ; Org is the default, set others here
(setq denote-prompts '(title keywords))
(setq denote-excluded-directories-regexp nil)
(setq denote-excluded-keywords-regexp nil)

;; Pick dates, where relevant, with Org's advanced interface:
(setq denote-date-prompt-use-org-read-date t)

;; Read this manual for how to specify `denote-templates'.  We do not
;; include an example here to avoid potential confusion.

;; We do not allow multi-word keywords by default.  The author's
;; personal preference is for single-word keywords for a more rigid
;; workflow.
(setq denote-allow-multi-word-keywords t)

(setq denote-date-format nil) ; read doc string

;; By default, we do not show the context of links.  We just display
;; file names.  This provides a more informative view.
(setq denote-backlinks-show-context t)

;; Also see `denote-link-backlinks-display-buffer-action' which is a bit
;; advanced.

;; If you use Markdown or plain text files (Org renders links as buttons
;; right away)
(add-hook 'find-file-hook #'denote-link-buttonize-buffer)

;; We use different ways to specify a path for demo purposes.
(setq denote-dired-directories
      (list denote-directory
            (thread-last denote-directory (expand-file-name "attachments"))
            (expand-file-name "~/Documents/books")))

;; Generic (great if you rename files Denote-style in lots of places):
;; (add-hook 'dired-mode-hook #'denote-dired-mode)
;;
;; OR if only want it in `denote-dired-directories':
(add-hook 'dired-mode-hook #'denote-dired-mode-in-directories)

;; Denote DOES NOT define any key bindings.  This is for the user to
;; decide.  For example:
(let ((map global-map))
  (define-key map (kbd "C-c n n") #'denote)
  (define-key map (kbd "C-c n c") #'denote-region) ; "contents" mnemonic
  (define-key map (kbd "C-c n N") #'denote-type)
  (define-key map (kbd "C-c n d") #'denote-date)
  (define-key map (kbd "C-c n z") #'denote-signature) ; "zettelkasten" mnemonic
  (define-key map (kbd "C-c n s") #'denote-subdirectory)
  (define-key map (kbd "C-c n t") #'denote-template)
  ;; If you intend to use Denote with a variety of file types, it is
  ;; easier to bind the link-related commands to the `global-map', as
  ;; shown here.  Otherwise follow the same pattern for `org-mode-map',
  ;; `markdown-mode-map', and/or `text-mode-map'.
  (define-key map (kbd "C-c n i") #'denote-link) ; "insert" mnemonic
  (define-key map (kbd "C-c n I") #'denote-add-links)
  (define-key map (kbd "C-c n b") #'denote-backlinks)
  (define-key map (kbd "C-c n f f") #'denote-find-link)
  (define-key map (kbd "C-c n f b") #'denote-find-backlink)
  ;; Note that `denote-rename-file' can work from any context, not just
  ;; Dired bufffers.  That is why we bind it here to the `global-map'.
  (define-key map (kbd "C-c n r") #'denote-rename-file)
  (define-key map (kbd "C-c n R") #'denote-rename-file-using-front-matter))

;; Key bindings specifically for Dired.
(let ((map dired-mode-map))
  (define-key map (kbd "C-c C-d C-i") #'denote-link-dired-marked-notes)
  (define-key map (kbd "C-c C-d C-r") #'denote-dired-rename-marked-files)
  (define-key map (kbd "C-c C-d C-R") #'denote-dired-rename-marked-files-using-front-matter))

(with-eval-after-load 'org-capture
  (setq denote-org-capture-specifiers "%l\n%i\n%?")
  (add-to-list 'org-capture-templates
               '("n" "New note (with denote.el)" plain
                 (file denote-last-path)
                 #'denote-org-capture
                 :no-save t
                 :immediate-finish nil
                 :kill-buffer t
                 :jump-to-captured t)))

;; Also check the commands `denote-link-after-creating',
;; `denote-link-or-create'.  You may want to bind them to keys as well.

;; If you want to have Denote commands available via a right click
;; context menu, use the following and then enable
;; `context-menu-mode'.
(add-hook 'context-menu-functions #'denote-context-menu)


;;diminish
(use-package diminish)
(require 'diminish)
(diminish 'rainbow-mode)                                       ; Hide lighter from mode-line
(diminish 'rainbow-mode " Rbow")                               ; Replace rainbow-mode lighter with " Rbow"
(diminish 'rainbow-mode 'rainbow-mode-lighter)                 ; Use raingow-mode-lighter variable value
(diminish 'rainbow-mode '(" " "R-" "bow"))                     ; Replace rainbow-mode lighter with " R-bow"
(diminish 'rainbow-mode '((" " "R") "/" "bow"))                ; Replace rainbow-mode lighter with " R/bow"
(diminish 'rainbow-mode '(:eval (format " Rbow/%s" (+ 2 3))))  ; Replace rainbow-mode lighter with " Rbow/5"
(diminish 'rainbow-mode                                        ; Replace rainbow-mode lighter with greened " Rbow"
  '(:propertize " Rbow" face '(:foreground "green")))
(diminish 'rainbow-mode                                        ; If rainbow-mode-mode-linep is non-nil " Rbow/t"
  '(rainbow-mode-mode-linep " Rbow/t" " Rbow/nil"))
(diminish 'rainbow-mode '(3 " Rbow" "/" "s"))                  ; Replace rainbow-mode lighter with " Rb"


;; dired+
;; Open dired folders in same buffer
;; (put 'dired-find-alternate-file 'disabled nil)

;; (setq dired-guess-shell-alist-user `(("\\.png\\'" "gimp")
;;                                      ("\\.jpe?g\\'" "gimp")
;;                                      ("\\.mp4\\'" "vlc")))

(use-package dired+
;; nil
;;  :commands
;;  (dired dired-jump)
  :custom
  (dired-listing-switches
   "-goah --group-directories-first --time-style=long-iso")
  (dired-dwim-target t)
  (delete-by-moving-to-trash t))
  

;; Hide hidden files
(use-package dired-hide-dotfiles
  :hook
  (dired-mode . dired-hide-dotfiles-mode)
  :bind
  (:map dired-mode-map ("." . dired-hide-dotfiles-mode)))

;; Keep folders clean (create new directory when not yet existing)
;;(make-directory (expand-file-name "backups/" user-emacs-directory) t)
(setq-default backup-directory-alist
              `(("." . ,(expand-file-name "backups/" user-emacs-directory)))
              create-lockfiles nil)  ; No lock files

;;; Configure outline minor modes
;; Less crazy key bindings for outline-minor-mode
(setq outline-minor-mode-prefix "\C-c\C-o")
;; load outline-magic along with outline-minor-mode
(add-hook 'outline-minor-mode-hook 
      (lambda ()
        (require 'outline-magic)
        (define-key outline-minor-mode-map "\C-c\C-o\t" 'outline-cycle)
       (define-key outline-minor-mode-map (kbd "<backtab>") 'outline-cycle)))

;; revert dired and other buffers
(setq global-auto-revert-non-file-buffers t)


(use-package drag-stuff)
(drag-stuff-mode t)
(drag-stuff-global-mode 1)
(drag-stuff-define-keys)


(use-package editorconfig)


(use-package electric)


(use-package emojify
  :hook (after-init . global-emojify-mode))
(add-hook 'after-init-hook #'global-emojify-mode)


(use-package expand-region)

(use-package f)

(use-package fold)


(use-package flycheck)


(use-package format-all)


;; Use keybindings
(use-package grip-mode
  :bind (:map markdown-mode-command-map
         ("g" . grip-mode)))

;; Or using hooks
(use-package grip-mode
  :hook ((markdown-mode org-mode) . grip-mode))


(use-package hideshowvis)
;;  (add-hook 'prog-mode-hook 'highshowvis-enable)


(use-package hl-todo)


;; helpful
(use-package helpful)
  ;; Note that the built-in `describe-function' includes both functions
  ;; and macros. `helpful-function' is functions only, so we provide
  ;; `helpful-callable' as a drop-in replacement.
(global-set-key (kbd "C-h f") #'helpful-callable)
  
(global-set-key (kbd "C-h v") #'helpful-variable)
(global-set-key (kbd "C-h k") #'helpful-key)
(global-set-key (kbd "C-h x") #'helpful-command)
  
  ;; Lookup the current symbol at point. C-c C-d is a common keybinding
  ;; for this in lisp modes.
(global-set-key (kbd "C-c C-d") #'helpful-at-point)
  
  ;; Look up *F*unctions (excludes macros).
  ;;
  ;; By default, C-h F is bound to `Info-goto-emacs-command-node'. Helpful
  ;; already links to the manual, if a function is referenced there.
(global-set-key (kbd "C-h F") #'helpful-function)
  
(setq counsel-describe-function-function #'helpful-callable)
(setq counsel-describe-variable-function #'helpful-variable)


(use-package ibuffer)

;; helm
(use-package helm)
(require 'helm)

;; The default "C-x c" is quite close to "C-x C-c", which quits Emacs.
;; Changed to "C-c h". Note: We must set "C-c h" globally, because we
;; cannot change `helm-command-prefix-key' once `helm-config' is loaded.
(global-set-key (kbd "C-c h") 'helm-command-prefix)
(global-unset-key (kbd "C-x c"))

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebind tab to run persistent action
(define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB work in terminal
(define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

(when (executable-find "curl")
  (setq helm-google-suggest-use-curl-p t))

(setq helm-split-window-in-side-p           t ; open helm buffer inside current window, not occupy whole other window
      helm-move-to-line-cycle-in-source     t ; move to end or beginning of source when reaching top or bottom of source.
      helm-ff-search-library-in-sexp        t ; search for library in `require' and `declare-function' sexp.
      helm-scroll-amount                    8 ; scroll 8 lines other window using M-<next>/M-<prior>
      helm-ff-file-name-history-use-recentf t
      helm-echo-input-in-header-line t)

(defun spacemacs//helm-hide-minibuffer-maybe ()
  "Hide minibuffer in Helm session if we use the header line as input field."
  (when (with-helm-buffer helm-echo-input-in-header-line)
    (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
      (overlay-put ov 'window (selected-window))
      (overlay-put ov 'face
                   (let ((bg-color (face-background 'default nil)))
                     `(:background ,bg-color :foreground ,bg-color)))
      (setq-local cursor-type nil))))


(add-hook 'helm-minibuffer-set-up-hook
          'spacemacs//helm-hide-minibuffer-maybe)

(setq helm-autoresize-max-height 0)
(setq helm-autoresize-min-height 20)
(helm-autoresize-mode 1)

(helm-mode 1)


;; ido mode
(use-package ido-vertical-mode
  :config
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (setq ido-vertical-define-keys 'C-n-and-C-p-only)      
  :init
  (ido-mode 1)
  (ido-vertical-mode 1))


;; indent guide
(use-package highlight-indent-guides
  :defer t
  :hook (prog-mode . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-character ?\|)
  (setq highlight-indent-guides-responsive 'top))

(use-package hyperbole)


(use-package ialign)
(require 'ialign)
(global-set-key (kbd "C-x l") #'ialign)

(use-package iedit)
(require 'iedit)
(defun iedit-dwim (arg)
  "Starts iedit but uses \\[narrow-to-defun] to limit its scope."
  (interactive "P")
  (if arg
      (iedit-mode)
    (save-excursion
      (save-restriction
        (widen)
        ;; this function determines the scope of `iedit-start'.
        (if iedit-mode
            (iedit-done)
          ;; `current-word' can of course be replaced by other
          ;; functions.
          (narrow-to-defun)
          (iedit-start (current-word) (point-min) (point-max)))))))
(global-set-key (kbd "C-;") 'iedit-dwim)

(use-package indent-guide)
;; Require this script
(require 'indent-guide)

;; and call command "M-x indent-guide-mode".

;; If you want to enable indent-guide-mode automatically,
;; call "indent-guide-global-mode" function.
(indent-guide-global-mode)

;; Column lines are propertized with "indent-guide-face". So you may
;; configure this face to make guides more pretty in your colorscheme.
(set-face-background 'indent-guide-face "dimgray")

;; You may also change the character for guides.
(setq indent-guide-char ":")


(use-package json-mode)


;; link-hint
(use-package link-hint
  :ensure t
  :bind
  ("C-c l o" . link-hint-open-link)
  ("C-c l c" . link-hint-copy-link))

;; Use chromium to open urls
;;(setq browse-url-browser-function 'browse-url-chromium)

;; Use firefox to open urls
;;(setq browse-url-browser-function 'browse-url-firefox)

;; Use qutebrowser to open urls
;;(setq browse-url-browser-function 'browse-url-generic)
;;(setq browse-url-generic-program "qutebrowser")
;; Open urls in a new tab instead of window; can also be set in the config file
;;(setq browse-url-generic-args '("--target" "tab"))


;; lsp-mode
(use-package lsp-mode
  :defer t
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config
  (setq lsp-headerline-breadcrumb-enable nil))

(use-package dap-mode
  :after lsp-mode
  :ensure t
  :defer t)

(use-package lsp-ui
  :commands lsp-ui-mode
  :config
  (setq lsp-ui-doc-enable nil)
  (setq lsp-ui-doc-header t)
  (setq lsp-ui-doc-include-signature t)
  (setq lsp-ui-doc-border (face-foreground 'default))
  (setq lsp-ui-sideline-show-code-actions t)
  (setq lsp-ui-sideline-delay 0.05))


(use-package magit)
;; Install package `delta`
;; Windows: `choco install delta`
;; Nix: `delta`
(use-package magit-delta
  :hook (magit-mode . magit-delta-mode))
(use-package git-timemachine)
(use-package magit-section)

;; marginalia
(use-package marginalia
  :config
  (marginalia-mode))

  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
 ;; :bind (:map minibuffer-local-map
 ;;             ("M-A" . marginalia-cycle))

(use-package embark
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'
  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc.  You may adjust the Eldoc
  ;; strategy, if you want to see the documentation from multiple providers.
  (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)
  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
           '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
             nil
             (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))


;;(use-package linum-relative)
;;  (require linum-relative)
;;  (linum-on)
     ;; Use `display-line-number-mode` as linum-mode's backend for smooth performance
;;	(setq linum-relative-backend 'display-line-numbers-mode)


;; markdown mode
(setq markdown-command 
      "pandoc -f markdown -t html -s --mathjax --highlight-style=pygments")

(use-package markdown-mode
  :mode ("README\\.md\\'" . gfm-mode)
  :init (setq markdown-command "pandoc")
  :bind (:map markdown-mode-map
         ("C-c C-e" . markdown-do)))

;; Use markdown-mode for files with .markdown or .md extensions
(setq
 markdown-enable-math t
 markdown-fontify-code-blocks-natively t)
(add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
(add-hook 'markdown-mode-hook 'turn-on-orgtbl)
(add-hook 'message-mode-hook #'turn-on-orgtbl)
(when (executable-find "pandoc")
  (add-hook 'markdown-mode-hook 'pandoc-mode))

;; Set custom markdown preview function
(setq markdown-live-preview-window-function #'my-markdown-preview-function)

;; always open the preview window at the right
(setq markdown-split-window-direction 'right)
;; always open the preview window at the bottom
(setq markdown-split-window-direction 'below)

;; delete exported HTML file after markdown-live-preview-export is called
(setq markdown-live-preview-delete-export 'delete-on-export)


;; minimap
(use-package minimap)
(minimap-create)
(setq minimap-window-location 'right)
(custom-set-faces
  '(minimap-active-region-background
    ((((background dark)) (:background "#551a8b"))
     (t (:background "#D3D3D3222222")))
    "Face for the active region in the minimap.
By default, this is only a different background color."
    :group 'minimap))


(use-package mood-line
  :if window-system
  :init
  (mood-line-mode))


(use-package move-text)
(move-text-default-bindings)

(use-package multiple-cursors)
(require 'multiple-cursors)
(global-set-key (kbd "C-|") 'mc/edit-lines)
(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)
(define-key mc/keymap (kbd "<return>") nil)


(use-package nix-mode
  :mode "\\.nix\\'")

(use-package mode-icons)
  (mode-icons-mode)


(use-package mw-thesaurus
  :defer t
  :commands mw-thesaurus-lookup-dwim
  :hook (mw-thesaurus-mode . variable-pitch-mode)
  :config
  (map! :map mw-thesaurus-mode-map [remap evil-record-macro] #'mw-thesaurus--quit)

  ;; window on the right side
  (add-to-list
   'display-buffer-alist
   `(,mw-thesaurus-buffer-name
     (display-buffer-reuse-window
      display-buffer-in-direction)
     (direction . right)
     (window . root)
     (window-width . 0.3))))


;; orderless
(use-package orderless
  :init
  ;; Configure a custom style dispatcher (see the Consult wiki)
  ;; (setq orderless-style-dispatchers '(+orderless-dispatch)
  ;;       orderless-component-separator #'orderless-escapable-split-on-space)
  (setq completion-styles '(orderless basic)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))))


;; org-mode
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)

(setq org-agenda-files (append
                        (file-expand-wildcards "~/dox/org/*.org")))

(setq org-todo-keywords
      '((sequence "TODO(t)"  "HOLD(h)"  "|" "DONE(d)" "FAIL(f)")))

(require 'org-mouse)

(defun my-org-autodone (n-done n-not-done)
  "Switch entry to DONE when all subentries are done, to TODO otherwise."
  (let (org-log-done org-log-states)   ; turn off logging
    (org-todo (if (= n-not-done 0) "DONE" "TODO"))))

(add-hook 'org-after-todo-statistics-hook 'my-org-autodone)

(setq-default org-src-fontify-natively t)


;; (use-package org-bookmark-heading

(use-package org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))

(use-package org-auto-tangle)
(require 'org-auto-tangle)
(add-hook 'org-mode-hook 'org-auto-tangle-mode)

(use-package org-pandoc-import
  :straight (:host github
             :repo "tecosaur/org-pandoc-import"
             :files ("*.el" "filters" "preprocessors")))

;; (use-package org-page)

 ;; (use-package org-ql) ;;#

;; (use-package org-protocol)
(require 'org-protocol)
(use-package org-protocol-capture-html)
;; requires s.el, see below


;;(use-package org-super-agenda)

(use-package org-sticky-header)

(use-package org-web-tools)


;; org-roam
(setq org-directory (concat (getenv "HOME") "/Documents/org-roam/"))
(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory (file-truename "/Documents/org-roam/"))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n g" . org-roam-graph)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ;; Dailies
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  ;; If you're using a vertical completion framework, you might want a more informative completion interface
  (setq org-roam-node-display-template (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))
  
(use-package emacsql)
(use-package emacsql-sqlite)


;; oragami
(use-package origami)
(require 'origami)
(use-package dash)


(use-package outline-magic)

(use-package page-break-lines)
(page-break-lines-mode)

(use-package parinfer-rust-mode
    :hook emacs-lisp-mode
    :init
    (setq parinfer-rust-auto-download t))


(use-package popper
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints


(use-package powerline)
(require 'powerline)
(powerline-default-theme)


(use-package prettier-js
  :config
  (setq prettier-js-args '(
                           "--trailing-comma" "es5"
                           "--single-quote" "true"
                           "--print-width" "120"
                           "--tab-width" "4"
                           "--use-tabs" "false"
                           "--jsx-bracket-same-line" "false"
                           "--stylelint-integration" "true")))
                           


(use-package projectile
  :defer t
  :config
  ;; Enable Projectile globally
  (setq projectile-completion-system 'ido)
  (setq ido-enable-flex-matching t)
  (projectile-mode 1))


(use-package rainbow-delimiters)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)

(use-package restart-emacs)


(use-package  rotate-text)


(use-package s)


(use-package smartparens
  :init
  (smartparens-global-mode))


(use-package smex
  :init (smex-initialize)
  :bind
  ("M-x" . smex))

;; spell-fu
(use-package spell-fu)

(add-hook 'org-mode-hook
  (lambda ()
    (setq spell-fu-faces-exclude
     '(org-block-begin-line
       org-block-end-line
       org-code
       org-date
       org-drawer org-document-info-keyword
       org-ellipsis
       org-link
       org-meta-line
       org-properties
       org-properties-value
       org-special-keyword
       org-src
       org-tag
       org-verbatim))
    (spell-fu-mode)))

;;(add-hook 'emacs-lisp-mode-hook
;;  (lambda ()
;;    (spell-fu-mode)))


(use-package svg-tag-mode)


(use-package swiper
  :bind (("M-s" . counsel-grep-or-swiper)))


(use-package syntax)


;; treemacs
(use-package treemacs
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                  (if treemacs-python-executable 3 0)
          treemacs-deferred-git-apply-delay        0.5
          treemacs-directory-name-transformer      #'identity
          treemacs-display-in-side-window          t
          treemacs-display-current-project-exclusively  t ;;
          treemacs-eldoc-display                   'simple
          treemacs-file-event-delay                2000
          treemacs-file-extension-regex            treemacs-last-period-regex-value
          treemacs-file-follow-delay               0.2
          treemacs-file-name-transformer           #'identity
          treemacs-follow-after-init               t
          treemacs-expand-after-init               t
          treemacs-find-workspace-method           'find-for-file-or-pick-first
          treemacs-git-command-pipe                ""
          treemacs-goto-tag-strategy               'refetch-index
          treemacs-header-scroll-indicators        '(nil . "^^^^^^")
          treemacs-hide-dot-git-directory          t
          treemacs-indentation                     2
          treemacs-indentation-string              " "
          treemacs-is-never-other-window           nil
          treemacs-max-git-entries                 5000
          treemacs-missing-project-action          'ask
          treemacs-move-forward-on-expand          nil
          treemacs-no-png-images                   nil
          treemacs-no-delete-other-windows         t
          treemacs-project-follow-cleanup          nil
          treemacs-project-follow-mode             t ;; 
          treemacs-persist-file                    (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-position                        'left
          treemacs-read-string-input               'from-child-frame
          treemacs-recenter-distance               0.1
          treemacs-recenter-after-file-follow      nil
          treemacs-recenter-after-tag-follow       nil
          treemacs-recenter-after-project-jump     'always
          treemacs-recenter-after-project-expand   'on-distance
          treemacs-litter-directories              '("/node_modules" "/.venv" "/.cask")
          treemacs-project-follow-into-home        nil
          treemacs-show-cursor                     nil
          treemacs-show-hidden-files               t
          treemacs-silent-filewatch                nil
          treemacs-silent-refresh                  nil
          treemacs-sorting                         'alphabetic-asc
          treemacs-select-when-already-in-treemacs 'move-back
          treemacs-space-between-root-nodes        t
          treemacs-tag-follow-cleanup              t
          treemacs-tag-follow-delay                1.5
          treemacs-text-scale                      nil
          treemacs-user-mode-line-format           nil
          treemacs-user-header-line-format         nil
          treemacs-wide-toggle-width               70
          treemacs-width                           35
          treemacs-width-increment                 1
          treemacs-width-is-initially-locked       t
          treemacs-workspace-switch-cleanup        nil)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode 'always)
    (when treemacs-python-executable
      (treemacs-git-commit-diff-mode t))

    (pcase (cons (not (null (executable-find "git")))
                 (not (null treemacs-python-executable)))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    (treemacs-hide-gitignored-files-mode nil))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t d"   . treemacs-select-directory)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(with-eval-after-load 'treemacs
  (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action))

(add-hook 'emacs-startup-hook 'treemacs)

(use-package treemacs-projectile
  :after (treemacs projectile))

(use-package treemacs-icons-dired
  :hook (dired-mode . treemacs-icons-dired-enable-once))

(use-package treemacs-magit
  :after (treemacs magit))

(use-package treemacs-persp ;;treemacs-perspective if you use perspective.el vs. persp-mode
  :after (treemacs persp-mode) ;;or perspective vs. persp-mode
  :config (treemacs-set-scope-type 'Perspectives))

(use-package treemacs-tab-bar ;;treemacs-tab-bar if you use tab-bar-mode
  :after (treemacs)
  :config (treemacs-set-scope-type 'Tabs));;


;; undo-fu
(use-package undo-fu
  :config
  (global-unset-key (kbd "C-z"))
  (global-set-key (kbd "C-z")   'undo-fu-only-undo)
  (global-set-key (kbd "C-S-z") 'undo-fu-only-redo))
(setq undo-limit 67108864) ; 64mb.
(setq undo-strong-limit 100663296) ; 96mb.
(setq undo-outer-limit 1006632960) ; 960mb.

(use-package undo-fu-session
  :config
  (setq undo-fu-session-incompatible-files '("/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'")))

(undo-fu-session-global-mode)
  

;; vertico
(use-package vertico
  :init
  (vertico-mode))

  ;; Different scroll margin
  ;; (setq vertico-scroll-margin 0)

  ;; Show more candidates
  ;; (setq vertico-count 20)

  ;; Grow and shrink the Vertico minibuffer
  ;; (setq vertico-resize t)

  ;; Optionally enable cycling for `vertico-next' and `vertico-previous'.
  ;; (setq vertico-cycle t)
  

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))


(use-package vimish-fold)

(use-package visual-regexp)
 ;; (add-to-list 'load-path "folder-in-which-visual-regexp-files-are-in/") ;; if the files are not already in the load path
(require 'visual-regexp)
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-c q") 'vr/query-replace)
  ;; if you use multiple-cursors, this is for you:
(define-key global-map (kbd "C-c m") 'vr/mc-mark)

(use-package visual-regexp-steroids)
  ;; if the files are not already in the load path
  ;; (add-to-list 'load-path "folder-to/visual-regexp/")
  ;; (add-to-list 'load-path "folder-to/visual-regexp-steroids/")
(require 'visual-regexp-steroids)
(define-key global-map (kbd "C-c r") 'vr/replace)
(define-key global-map (kbd "C-c q") 'vr/query-replace)
  ;; if you use multiple-cursors, this is for you:
(define-key global-map (kbd "C-c m") 'vr/mc-mark)
  ;; to use visual-regexp-steroids's isearch instead of the built-in regexp isearch, also include the following lines:
(define-key esc-map (kbd "C-r") 'vr/isearch-backward) ;; C-M-r
(define-key esc-map (kbd "C-s") 'vr/isearch-forward) ;; C-M-s


(use-package wgrep) ;; Use with consult-grep via embark-export.


(use-package which-key
  :config
  (which-key-mode))


(use-package xeft)

(use-package yafolding)

(defvar yafolding-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "<C-S-return>") #'yafolding-hide-parent-element)
    (define-key map (kbd "<C-M-return>") #'yafolding-toggle-all)
    (define-key map (kbd "<C-return>") #'yafolding-toggle-element)
    map))
  
(global-set-key (kbd "s-d y") 'yafolding-discover)
  
(add-hook 'prog-mode-hook
          (lambda () (yafolding-mode)))
  
(require 'yafolding)
(define-key yafolding-mode-map (kbd "<C-S-return>") nil)
(define-key yafolding-mode-map (kbd "<C-M-return>") nil)
(define-key yafolding-mode-map (kbd "<C-return>") nil)
(define-key yafolding-mode-map (kbd "C-c <C-M-return>") 'yafolding-toggle-all)
(define-key yafolding-mode-map (kbd "C-c <C-S-return>") 'yafolding-hide-parent-element)
(define-key yafolding-mode-map (kbd "C-c <C-return>") 'yafolding-toggle-element)


(use-package yascroll)
(global-yascroll-bar-mode 1)


(use-package yasnippet)

;; https://lists.gnu.org/archive/html/emacs-orgmode/2013-10/msg00079.html
;; https://lists.gnu.org/archive/html/emacs-orgmode/2013-10/msg00072.html
;; Use fixed width mode in tables.
;;(set-face-attribute 'variable-pitch nil :family "DejaVu Serif")
;;(set-face-attribute 'variable-pitch nil :slant 'italic)
;;(set-face-attribute 'variable-pitch nil :height 120)
;; (add-hook 'text-mode-hook 'turn-on-auto-fill)
;;(add-hook 'text-mode-hook 'variable-pitch-mode)
;;(add-hook 'org-mode-hook
;;(lambda ()
;; Enable fill column indicator
;; (fci-mode t)
;; Turn off line numbering, it makes org so slow
;; (linum-mode -1)
;; Set fill column to 79
;;(setq fill-column 79)
;; Enable automatic line wrapping at fill column
;;(auto-fill-mode t)))
;;(defun my-adjoin-to-list-or-symbol (element list-or-symbol)
;;(let ((list (if (not (listp list-or-symbol))
;;(list list-or-symbol)
;;list-or-symbol)))
;;(require 'cl-lib)
;;(cl-adjoin element list)))
;;(mapc
;;(lambda (face)
;;(set-face-attribute
;;face nil
;;:inherit
;;(my-adjoin-to-list-or-symbol
;;'fixed-pitch
;;(face-attribute face :inherit))))
;;(list 'org-code 'org-block 'org-table 'org-date
;;      'org-link 'org-footnote))
