{ config, pkgs, ... }:  

{
       home.file = {
       # NAME
       # "/PATH/TO/USERDIRECTORY/".source = ./PATH/TO/FLAKE/FOLDER_NAME
       
       # Alacritty
       ".config/alacritty/alacritty.yml".text = ''
        {window: {opacity: 0.8}, font: {normal: {family: tamsyn, style: Regular}}}
       '';

        # Bash
        ".bashrc".source = ../config/.bashrc;   

        # Dunst
        ".config/dunst/dunstrc".source = ./dunst/dunstrc;

        # Emacs
        # TODO Would like to declare emacs configs at some point
        #".config/emacs".source = ./emacs;

        # Hyprland
        ".config/hypr".source = ./hypr;

        # Kitty
        ".config/kitty/kitty.conf".source = ./kitty/kitty.conf;
        ".config/kitty/current-theme.conf".source = ./kitty/current-theme.conf;
                  
        # Rofi
        ".config/rofi/config".source = ./rofi;

        # TODO Starship
        ".config/starship.toml".source = ./starship/starship.toml;

        # VSCodium
        ".config/VSCodium/User/snippets/Header.code-snippets".source = ./vscodium/snippets/global/header.conf;

        # Waybar
        ".config/waybar/config".source = ./waybar;
     };
}