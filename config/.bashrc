# Aliases


# Alias: feh
alias fss="feh -FZzrxD "
alias fxf="feh -YFZzrxD .02 "      # Slideshow displaying rapid images
alias fxm="feh -YFZzrxD 3 "        # Slideshow displaying 3 second randomized images
alias fxs="feh -YFZzrxD 20 "       # Slideshow displaying 20 second randomized images
alias fxr="feh -YFZzrx "           # Slideshow displaying randomized images, not timed
alias fxl="feh -YFZrxS mtime "     # Slideshow displaying last images modified


# Alias: Launch flatpaks (legacy reference)
#alias brave="com.brave.Browser"


# Alias: Gnome Menu: Alphabetize
alias alpha-gnome="gsettings reset org.gnome.shell app-picker-layout"


# Alias: OpenBB
alias obb="docker run -it --rm ghcr.io/openbb-finance/openbbterminal-poetry:latest"
alias obb2="cd /home/$USER/Software/OpenBB && docker compose run openbb"


# Alias: Nano
alias bash="sudo nano .bashrc"
alias sn="sudo nano "   

# Alias: NixOS
alias clean="sudo nix-collect-garbage -d && sudo nix-store --optimise"
alias repair="sudo nix-store --verify --check-contents --repair"
alias opt="sudo nix-store optimise"
alias flkup="cd && cd flake && nix flake update && sudo nixos-rebuild switch --flake .# && sleep 10 && flatpak update && flatpak uninstall --unused" 


# Alias: Terminal
alias pipes="pipes.sh"
alias starwars="telnet towel.blinkenlights.nl"
alias wttr="curl wttr.in"


# Waydroid
alias wss="waydroid session stop"



# Programs


# Custom Bash Prompt (use in case you decide to stop using starship)
#PS1='\n\[\e[0m\]\@ \[\e[0;1;38;5;45m\]\u\[\e[0m\]@\[\e[0;1;38;5;98m\]\h \[\e[0m\]\$\n\[\e[0m\](\[\e[0;2;38;5;231m\]\w\[\e[0m\])\[\e[0;5;96m\]>\[\e[0;5;36m\]>\[\e[0;5;35m\]>\[\e[0m\]'


# Starship
eval "$(starship init bash)"


# Enable Carapace
source <(carapace _carapace)


# Fetch on Startup
#neofetch
#fastfatch
#nitch


# Pokemon
krabby random 1 --no-title


# zoxide
eval "$(zoxide init bash)"