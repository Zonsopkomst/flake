{ config, pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    appimage-run              # Appimage Runner
    asciiquarium              # Terminal Aquarium      
    bash-completion           # Bash Autocomplete
    bat                       # cat like concatenate or display command
    bottom                    # System Monitor
    broot                     # Directory Navigator
    btop                      # Resource Monitor
    carapace                  # Argument Completer
    cbonsai                   # Terminal Bonsai Tree
    delta                     # syntax-highlighting pager for git
    difftastic                # diff replacement
    du-dust                   # du command replacement
    eza                       # ls like list command
    fd                        # find command
    feh                       # Image Viewer
    fzf                       # Fuzzy Finder in GO    
    git                       # Distributed Version Control System
    genact                    # Nonsense Activity Generator
    hollywood                 # Hollywood Melodrama Technobabble
    jq                        # filter data command
    krabby                    # Terminal Pokemon
    lavat                     # Terminal Lavalamp
    monolith                  # Single HTML Saver
    neo                       # Matrix Digital Rain
    neofetch                  # System Information
    nitch                     # Nim System Fetch
    nix-bash-completions      # Bash Autocomplete
    nms                       # Effect from 1992 Movie Sneakers
    nyancat                   # Terminal Nyancat
    ocrmypdf                  # OCR Text Layer to Scanned PDFs
    ouch                      # Obvious Unified Compression Helper
    pandoc                    # Markup Converter
    pipes                     # Animated Pipes Terminal Screensaver
    procs                     # Processes, `ps` replacement
    ranger                    # File Manager
    ripgrep                   # Temporarily using ripgrip until ripgrep-all is available again
    #ripgrep-all               # search command; NOTE: Failed to build, temp removing
    sd                        # find & replace command
    #shell-genie               # Natural Language Shell Command AI
    skim                      # Fuzzy Finder
    starship                  # Shell Prompt
    tealdeer                  # tldr command
    tokei                     # Code Statistics
    ugrep                     # Grep Replacement
    xcowsay                   # Graphical Cowsay          
    xorg.xkill                # Kill Windows w/ Mouse
    zoxide                    # fast cd directory command
  ];
}
