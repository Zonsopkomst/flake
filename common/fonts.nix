{ config, pkgs, ... }:

{
  fonts.fontDir.enable = true;
  
  fonts.packages = with pkgs; [
    comfortaa                  # Clean and modern font 
    corefonts                  # Microsoft Core Fonts
    emacs-all-the-icons-fonts  # Emacs all-the-icons-fonts
    roboto                     # Android Family of Fonts
    noto-fonts-cjk-sans        # CJK Family of Fonts
    noto-fonts-cjk-serif       # CJK Family of Fonts
    (nerdfonts.override { fonts = [ "FiraCode" "JetBrainsMono" "SpaceMono" "DaddyTimeMono" ]; })
  ];
}