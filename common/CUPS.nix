  { config, pkgs, ... }:
  
  {
    # Enable CUPS to print documents.
    services.printing.enable = true;

    # Enable support for SANE scanners.
    # Add "scanner" & "lp" to users `extraGroups` for access to printer & scanner.
    hardware.sane.enable = true;
  }