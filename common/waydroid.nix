{ config, pkgs, ... }:  

{
  virtualisation = {
    waydroid.enable = true;
    lxd.enable = true;
  };
}

## Instructions (verified)

# sudo rm -rf /var/lib/waydroid /home/.waydroid ~/waydroid ~/.share/waydroid ~/.local/share/applications/*aydroid* ~/.local/share/waydroid

# sudo waydroid init -s GAPPS -f
# [20:44:33] Downloading https://sourceforge.net/projects/waydroid/files/images/system/lineage/waydroid_x86_64/lineage-18.1-20230318-GAPPS-waydroid_x86_64-system.zip/download
# [Downloading] 858.39 MB/858.44 MB    12783.91 kbps(approx.)[20:46:00] Validating system image
# [20:46:01] Extracting to /var/lib/waydroid/images
# [20:46:21] Downloading https://sourceforge.net/projects/waydroid/files/images/vendor/waydroid_x86_64/lineage-18.1-20230318-MAINLINE-waydroid_x86_64-vendor.zip/download
# [Downloading] 165.74 MB/165.89 MB    12864.75 kbps(approx.)[20:46:40] Validating vendor image
# [Downloading] 165.89 MB/165.89 MB    12696.42 kbps(approx.)[20:46:40] Extracting to /var/lib/waydroid/images
 
# sudo systemctl start waydroid-container

# sudo journalctl -u waydroid-container
# Jan 22 18:04:52 nixos systemd[1]: Started Waydroid Container.

# waydroid session start
# [20:48:04] Android with user 0 is ready