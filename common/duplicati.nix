{ config, pkgs, ... }:  

# Enable and configure duplicati as a service

{
  services.duplicati = {
    enable = true;
    interface = "any";
  };
}