  { config, pkgs, ... }:
  
  {   
    # Enable HP printing drivers & scanner backend
    services.printing.drivers = [ pkgs.hplip ];
  
    hardware.sane.extraBackends = [ pkgs.hplipWithPlugin ];
  }