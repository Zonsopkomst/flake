{ config, pkgs, ... }:  

{
  boot.initrd.kernelModules = [ "amdgpu" ];
  services.xserver.videoDrivers = [ "amdgpu" ];

  # Enable OpenCL & Vulkan  
  #hardware.opengl.driSupport = true;
  hardware.graphics.enable32Bit = true;
  #hardware.opengl.driSupport32Bit = true;          # TODO 06/21/2024 remove line

  hardware.graphics.extraPackages = with pkgs; [
  #hardware.opengl.extraPackages = with pkgs; [     # TODO 06/21/2024 remove line
    rocm-opencl-icd
    rocm-opencl-runtime
    amdvlk
  ];
  
  # Enable HIP
  systemd.tmpfiles.rules = [
    ## Replaced `hip` in favor of `rocmPackages.clr`
    ##"L+    /opt/rocm/hip   -    -    -     -    ${pkgs.hip}"
    "L+    /opt/rocm/hip   -    -    -     -    ${pkgs.rocmPackages.clr}"

  ];

}