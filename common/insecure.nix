{ config, pkgs, ... }:  

{
  # Enable Insecure Packages
  nixpkgs.config.permittedInsecurePackages = [ 
    "electron-11.5.0" 
    "electron-22.3.27"
    "adobe-reader-9.5.5"
    "armcord-3.2.4"
  ];
}