{ config, pkgs, ... }:
 
{
 programs.vscode = {
  enable = true;
    package = pkgs.vscodium;
      extensions = with pkgs.vscode-extensions; [
        bbenoist.nix
        bierner.markdown-checkbox
        bierner.markdown-emoji
        bierner.emojisense
        bierner.markdown-mermaid
        donjayamanne.githistory
        file-icons.file-icons
        foam.foam-vscode
        jnoortheen.nix-ide
        piousdeer.adwaita-theme
        shd101wyy.markdown-preview-enhanced
        svsool.markdown-memo
        yzhang.markdown-all-in-one
        valentjn.vscode-ltex
      ] ++ pkgs.vscode-utils.extensionsFromVscodeMarketplace [
        {
          name = "find-jump";
          publisher = "usernamehw";
          version = "0.6.3";
          sha256 = "sha256-QXjQGn0w/tUTqQHXV+9/g3uLYdbdaSt5MuiwV9/tCi4=";
        }
        {
          name = "Lisp-Syntax";
          publisher = "slbtty";
          version = "0.2.1";
          sha256 = "sha256-Jos0MJBuFlbfyAK/w51+rblslNq+pHN8gl1T0/UcP0Q=";
        }        {
          name = "markdown-checkbox";
          publisher = "PKief";
          version = "1.8.1";
          sha256 = "sha256-GdSdJocRXqjMFZTOrGUCJHo33zaFWmeJHyssh8plS+w=";
        }
                {
          name = "markdown-converter";
          publisher = "manuth";
          version = "5.2.1";
          sha256 = "sha256-RGVZQnegnThUApa0IX7gonQ52GrLMabr1FkLhHout9w=";
        }
        {
          name = "markdown-table-prettify";
          publisher = "darkriszty";
          version = "3.6.0";
          sha256 = "sha256-FZTiNGSY+8xk3DJsTKQu4AHy1UFvg0gbrzPpjqRlECI=";
        }
        {
          name = "nimvscode";
          publisher = "nimsaem";
          version = "0.1.26";
          sha256 = "sha256-unxcnQR2ccsydVG3H13e+xYRnW+3/ArIuBt0HlCLKio=";
        } 
        {
          name = "org-mode";
          publisher = "vscode-org-mode";
          version = "1.0.0";
          sha256 = "sha256-o9CIjMlYQQVRdtTlOp9BAVjqrfFIhhdvzlyhlcOv5rY=";
        }
        {
          name = "vscode-yaml";
          publisher = "redhat";
          version = "1.12.0";
          sha256 = "sha256-r/me14KonxnQeensIYyWU4dQrhomc8h2ntYoiZ+Y7jE=";
        }
      ];
 };
}