{ config, pkgs, ... }:  

{
  # Enable the Docker Service
  # Note added "docker" group to user's extraGroups in configuration.nix
    virtualisation.docker.enable = true;
}