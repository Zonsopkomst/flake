{ config, lib, pkgs, modulesPath, ... }:

{
  systemd.extraConfig = ''
    DefaultTimeoutStopSec=10s
  '';
}