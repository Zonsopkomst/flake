{ config, pkgs, ... }:  

{
  environment.systemPackages = with pkgs; [
     dunst                            # Notification Daemon
     grim                             # Wayland Image Grab
     hyprpaper                        # Wallpaper Application for Wayland Compositors 
     rofi                             # Wayland Launcher/Menu
     slurp                            # screenshot
     waybar                           # Wayland Bar for Wlroots Based Compositors
     wbg                              # Wallpaper Application for Wayland Compositors
     wofi                             # Wayland Launcher/Menu 
  ];
}
