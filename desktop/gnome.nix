{ config, pkgs, ... }:

# Note, that certain packages, like gnucash, require dconf and must be set manually in the host's packages.nix or user's home-manager.nix

{  
  services.xserver.desktopManager.gnome.enable = true;
  services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];

  services.gnome.gnome-keyring.enable = true;

  # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
  systemd.services."getty@tty1".enable = false;
  systemd.services."autovt@tty1".enable = false;

  environment.systemPackages = with pkgs;[
    gnomeExtensions.appindicator                         # Gnome Appindicator
    gnome.gnome-shell-extensions                         # Gnome Shell Extensions
    gnomeExtensions.order-gnome-shell-extensions         # Gnome Shell Extensions Order
    gnome-tweaks                                         # Gnome Customization Tool  

    #gnomeExtensions.syncthing-icon
  ];

  environment.gnome.excludePackages = (with pkgs; [
    gedit             # text editor
    gnome-calculator  # calculator
    gnome-photos      # photos
    gnome-terminal    # terminal
    gnome-tour        # gnome tour
    epiphany          # web browser
    geary             # email reader
    loupe             # image viewer
    totem             # video player
  ]) ++ (with pkgs.gnome; [
    #cheese           # webcam tool
    gnome-music       # music player
    gnome-software    # software manager
    #evince           # document viewer
    #gnome-characters
  ]);
}