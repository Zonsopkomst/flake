{ config, pkgs, ... }:

{  
  services.xserver.desktopManager.plasma5.enable = true;
  programs.ssh.askPassword = pkgs.lib.mkForce "${pkgs.ksshaskpass.out}/bin/ksshaskpass";

  security.pam.services.$USER.enableKwallet = true;

    #kate                              # Text Editor
    #kde-gtk-config                    # KDE GTK Configuration
    #libsForQt5.applet-window-buttons  # KDE Applet
    #libsForQt5.breeze-gtk             # GTK Breeze Theming
    #libsForQt5.breeze-qt5             # QT5 Breeze Theming
    #libsForQt5.breeze-icons           # Breeze Icons
    #libsForQt5.filelight              # Storage Usage Application
    #libsForQt5.kaccounts-providers    # KDE Accounts#libsForQt5.qtstyleplugin-kvantum  # KDE Theming
    #libsForQt5.kdecoration            # KDE Theming
    #libsForQt5.powerdevil             # Power Manager
    #libsForQt5.sddm                   # SDDM Libraries
    #libsForQt5.sddm-kcm               # SDDM Libraries

}