{
  description = "Zonsopkomst's NixOS Flake to Manage Multiple Machines";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    hyprland.url = "github:hyprwm/Hyprland";
    nix-flatpak.url = "github:gmodena/nix-flatpak"; # unstable branch. Use github:gmodena/nix-flatpak/?ref=<tag> to pin releases
    itchpkgs.url = "github:nixos/nixpkgs/e1ee359d16a1886f0771cc433a00827da98d861c";
    ags.url = "github:Aylur/ags";


    # 06/17/2024 Addition  
    # TODO Stylix via Vimjoyer: 
      # https://github.com/vimjoyer/stylix-video
      # https://journix.dev/posts/ricing-linux-has-never-been-easier-nixos-and-stylix/
      # https://stylix.danth.me/options/nixos.html
      # https://www.youtube.com/watch?v=ljHkWgBaQWU
    stylix.url = "github:danth/stylix";

    ## = Rollback example of gnucash to v5.5
    ##gnucashpkgs.url = "github:nixos/nixpkgs/f4599feae4160285c5ce4e4364bd147df83e780c";
  };

 outputs = inputs@{ 
                    home-manager, 
                    hyprland, 
                    nixpkgs, 
                    nix-flatpak,
                    ...
                    }: {                    
     
 nixosConfigurations = {

###########      
## LOCAL ##
###########
      
      frija = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
        
        # Common Resources

        # Desktop
          ./desktop/gnome.nix
          ./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          #./common/kernel.zen.nix
          #./commmon/swappiness.nix
          
        # Common
          ./common/amd.nix
          #./common/bluetooth.nix
          #./common/CUPS.nix
          #./common/drivers-hp.nix
          ./common/duplicati.nix
          #./common/hyprland.nix
          ./common/pipewire.nix
          ./common/time.nix
          #./common/docker.nix
          ./common/flakeinit.nix
          #./common/flatpak.nix
          #./common/gaming.nix
          #./common/insecure.nix
          ./common/networking.nix
          ./common/systemd.nix
          ./common/terminal.nix
          ./common/unfree.nix
          #./common/waydroid.nix
          ./common/x11.nix

        # Host Specific
          ./hosts/local/frija/default.nix
          ./hosts/local/frija/hardware-configuration.nix
          ./hosts/local/frija/packages.nix

        # User Specific
          #./users/aspire/flatpak.nix
          ./users/aspire/home-manager.nix
          #./users/aspire/syncthing.nix

          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.aspire = import ./hosts/local/frija/home.nix;
          }

          nix-flatpak.nixosModules.nix-flatpak{
          }

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;}

        ];
      };

      thuner = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [

        # Common Resources

        # Desktop
          #./desktop/gnome.nix
          #./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          #./common/kernel.zen.nix
          #./commmon/swappiness.nix
          
        # Common          
          #./common/amd.nix
          #./common/bluetooth.nix
          #./common/CUPS.nix
          #./common/drivers-hp.nix
          #./common/duplicati.nix
          #./common/hyprland.nix
          #./common/pipewire.nix
          #./common/time.nix
          #./common/docker.nix
          #./common/flakeinit.nix
          #./common/flatpak.nix
          #./common/gaming.nix
          #./common/insecure.nix
          #./common/networking.nix
          #./common/systemd.nix
          #./common/terminal.nix
          #./common/unfree.nix
          #./common/waydroid.nix
          #./common/x11.nix

        # Host Specific
          ./hosts/local/thuner/default.nix          
          ./hosts/local/thuner/hardware-configuration.nix
          #./hosts/local/thuner/packages.nix

        # User Specific
          #./users/thuner/flatpak.nix
          #./users/thuner/home-manager.nix
          #./users/thuner/syncthing.nix               
          
          #home-manager.nixosModules.home-manager
          #{
          #  home-manager.useGlobalPkgs = true;
          #  home-manager.useUserPackages = true;
          #  home-manager.users.thuner = import ./hosts/local/tyr/home.nix;
          #}

          #nix-flatpak.nixosModules.nix-flatpak 
          #{
        #}

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;}

        ];
      };


      tyr = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [

        # Common Resources

        # Desktop
          ./desktop/gnome.nix
          ./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          ./common/kernel.zen.nix
          #./commmon/swappiness.nix
          
        # Common          
          #./common/amd.nix
          #./common/bluetooth.nix
          ./common/CUPS.nix
          #./common/drivers-hp.nix
          ./common/duplicati.nix
          #./common/hyprland.nix
          ./common/pipewire.nix
          ./common/time.nix
          ./common/docker.nix
          ./common/flakeinit.nix
          ./common/flatpak.nix
          ./common/gaming.nix
          ./common/insecure.nix
          ./common/networking.nix
          ./common/systemd.nix
          ./common/terminal.nix
          ./common/unfree.nix
          #./common/waydroid.nix
          ./common/x11.nix

        # Host Specific
          ./hosts/local/tyr/default.nix          
          ./hosts/local/tyr/hardware-configuration.nix
          ./hosts/local/tyr/packages.nix

        # User Specific
          ./users/leeuwarden/flatpak.nix
          ./users/leeuwarden/home-manager.nix
          ./users/leeuwarden/syncthing.nix               
          
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.leeuwarden = import ./hosts/local/tyr/home.nix;
            #home-manager.extraSpecialArgs = {inherit inputs;};
          }

          nix-flatpak.nixosModules.nix-flatpak{
          }

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;}
          
          # 06/17/2024 Addition
          #stylix.homeManagerModules.stylix
          #./desktop/stylix/stylix-tyr.nix

        ];
      };

     weda = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [
          
        # Common Resources

        # Desktop
          ./desktop/gnome.nix
          ./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          #./common/kernel.zen.nix
          #./commmon/swappiness.nix

        # Common                    
          ./common/amd.nix
          ./common/bluetooth.nix
          ./common/duplicati.nix
          ./common/CUPS.nix
          ./common/drivers-hp.nix
          #./common/hyprland.nix
          ./common/pipewire.nix
          ./common/time.nix
          ./common/docker.nix
          ./common/flakeinit.nix
          ./common/flatpak.nix
          ./common/gaming.nix
          ./common/insecure.nix
          ./common/networking.nix
          ./common/systemd.nix
          #./common/starship.nix
          ./common/terminal.nix
          ./common/unfree.nix
          ./common/waydroid.nix
          ./common/x11.nix

        # Host Specific
          ./hosts/local/weda/default.nix      
          ./hosts/local/weda/hardware-configuration.nix
          ./hosts/local/weda/packages.nix

        # User Specific
          #./users/zonsopkomst/appimage.nix  # 02/07/2024 Attempting to wrap appimages, staring with colorpicker
          ./users/zonsopkomst/flatpak.nix
          ./users/zonsopkomst/home-manager.nix
          ./users/zonsopkomst/syncthing.nix
          
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.zonsopkomst = import ./hosts/local/weda/home.nix;
            ##home-manager.extraSpecialArgs = {inherit inputs;};
          }

          nix-flatpak.nixosModules.nix-flatpak{
          }

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;
          #}

          #stylix.homeManagerModules.stylix
          
        ];
      };

#############     
## REMOTE ##
#############
     
      sintanne = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [

        # Common Resources

        # Desktop
          ./desktop/gnome.nix
          ./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          #./common/kernel.zen.nix
          #./commmon/swappiness.nix

        # Common          
          #./common/amd.nix
          #./common/bluetooth.nix
          ./common/CUPS.nix
          #./common/drivers-hp.nix
          ./common/duplicati.nix
          #./common/hyprland.nix
          ./common/pipewire.nix
          ./common/time.nix
          #./common/docker.nix
          ./common/flakeinit.nix
          ./common/flatpak.nix
          #./common/gaming.nix
          ./common/insecure.nix
          ./common/networking.nix
          ./common/systemd.nix
          ./common/terminal.nix
          ./common/unfree.nix
          #./common/waydroid.nix
          ./common/x11.nix

        # Host Specific
          ./hosts/remote/sintanne/default.nix          
          ./hosts/remote/sintanne/hardware-configuration.nix
          ./hosts/remote/sintanne/packages.nix

        # User Specific
          ./users/annette/flatpak.nix
          ./users/annette/home-manager.nix
          #./users/annette/hd.nix
          #./users/annette/syncthing.nix          
          
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.annette = import ./hosts/remote/sintanne/home.nix;
          }

          nix-flatpak.nixosModules.nix-flatpak{
          }

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;}
        ];
      };
      
      tii = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [

        # Common Resources

        # Desktop
          ./desktop/gnome.nix
          ./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          #./common/kernel.zen.nix
          #./commmon/swappiness.nix

        # Common          
          #./common/amd.nix
          ./common/bluetooth.nix
          ./common/CUPS.nix
          ./common/drivers-hp.nix
          ./common/duplicati.nix
          #./common/hyprland.nix
          ./common/pipewire.nix
          ./common/time.nix
          #./common/docker.nix
          ./common/flakeinit.nix
          ./common/flatpak.nix
          #./common/gaming.nix
          ./common/insecure.nix
          ./common/networking.nix
          ./common/systemd.nix
          ./common/terminal.nix
          ./common/unfree.nix
          #./common/waydroid.nix
          ./common/x11.nix

        # Host Specific
          ./hosts/remote/tii/default.nix          
          ./hosts/remote/tii/hardware-configuration.nix
          ./hosts/remote/tii/packages.nix

        # User Specific
          ./users/coleman/flatpak.nix
          ./users/coleman/home-manager.nix
          #./users/coleman/hd.nix
          ./users/coleman/syncthing.nix          
          
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.coleman = import ./hosts/remote/tii/home.nix;
          }

          nix-flatpak.nixosModules.nix-flatpak{
          }

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;}
        ];
      };

      tuisto = nixpkgs.lib.nixosSystem {
        system = "x86_64-linux";
        modules = [

        # Common Resources

        # Desktop
          ./desktop/gnome.nix
          ./display/gdm.nix
          #./desktop/kde.nix
          #./display/sddm.nix
          #./desktop/hyprland.nix

        # Kernels & RAM
          ./common/kernel.zen.nix
          #./commmon/swappiness.nix

        # Common          
          ./common/amd.nix
          #./common/bluetooth.nix
          ./common/CUPS.nix
          ./common/drivers-hp.nix
          ./common/duplicati.nix
          #./common/hyprland.nix
          ./common/pipewire.nix
          ./common/time.nix
          ./common/docker.nix
          ./common/flakeinit.nix
          ./common/flatpak.nix
          ./common/gaming.nix
          ./common/insecure.nix
          ./common/networking.nix
          ./common/systemd.nix
          ./common/terminal.nix
          ./common/unfree.nix
          #./common/waydroid.nix
          ./common/x11.nix

        # Host Specific
          ./hosts/remote/tuisto/default.nix          
          ./hosts/remote/tuisto/hardware-configuration.nix
          ./hosts/remote/tuisto/packages.nix

        # User Specific
          ./users/admin/flatpak.nix
          ./users/admin/home-manager.nix
          #./users/admin/hd.nix
          ./users/admin/syncthing.nix          
          
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.admin = import ./hosts/remote/tuisto/home.nix;
          }

          nix-flatpak.nixosModules.nix-flatpak{
          }

          #hyprland.nixosModules.default
          #{programs.hyprland.enable = true;}
        ];
      };
      
    };
  };
}
