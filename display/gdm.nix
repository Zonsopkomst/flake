{ config, pkgs, ... }:

{  
  services.xserver.displayManager.gdm.enable = true;
  security.pam.services.gdm.enableGnomeKeyring = true;
}