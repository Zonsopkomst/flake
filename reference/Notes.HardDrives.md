\
---
title: Hard Drives
type: Instructions
author: Eric Wagner / Chat GPT3.5 (BaiChat)
created: 2023-06-01
---
# Hard Drives
[[personal]] [[literature]] [[fiction]] [[georgerrmartin]] [[gameofthrones]]


To add two hard drives to a NixOS headless server, one for server data and the other for backing up, you will need to perform the following steps:

1. Install the hard drives: Start by physically installing the two hard drives into your server or virtual machine. Make sure that they are properly connected and powered.
 
2. Partition the hard drives: Use a tool like fdisk or parted to partition the two hard drives as desired. You can create separate partitions for the server data and backup data on each drive.
 
3. Format the partitions: Once the partitions have been created, format them with the desired filesystems using commands like mkfs.ext4, mkfs.xfs, or mkfs.btrfs.
 
4. Mount the partitions: Create directories where you want to mount the partitions and mount them using the mount command or by adding entries to /etc/fstab. For example, you could mount the first partition of the first hard drive at /mnt/data and the first partition of the second hard drive at /mnt/backup.
 
5. Configure the system: Use the NixOS configuration files to configure the system to use the new hard drives. You can specify filesystem types, mount points, and other relevant details in the configuration.nix file.
 
6. Test the system: After the system has been configured and rebuilt, test it to make sure everything is working as expected. This can include creating test files on the server data partition and verifying that they are accessible, as well as testing the backup process to make sure that data is being copied correctly to the backup drive.
 
7. By following these steps, you should be able to add two hard drives, one for server data and the other for backing up, to your NixOS headless server. Keep in mind that the exact steps may vary depending on your specific hardware and needs.