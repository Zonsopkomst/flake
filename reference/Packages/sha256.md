# How to Obtain the SHA256 of a File
Use the following in place of the SHA256 hash:

0000000000000000000000000000000000000000000000000000

Nix will fail to build, but should give you the correct SHA256 hash.