{ config, pkgs, ... }:

  # The following is a list of packages that I regularly use on different machines with brief discriptions of what function they serve

{
  # GLOBAL PACKAGES
    
  # Fonts
    fonts.fontDir.enable = true;
    fonts.packages = with pkgs; [
      corefonts                          # Microsoft Core Fonts
      font-awesome                       # Scalable vector icons
      helvetica-neue-lt-std              # Helvetica Neue LT Std font
      maple-mono                         # Monospace Font 
      roboto                             # Android Family of Fonts
      tamsyn                             # Monospace Bitmap Font for Programmers  
      (nerdfonts.override { fonts = [ "FiraCode" "SpaceMono" ]; })
    ]

  # Power Management
  services.tlp.enable = true;

  # GLOBAL & HOME-MANAGER PACKAGES
    
  # Cartography
    josm                              # Extensible Editor for OpenStreetMap  
    qgis                              # Geographic Information System

  # Communication
    armcord                           # Discord Client
    dino                              # XMPP Client
    discord                           # Discord Client
    element-desktop                   # Matrix Client
    gajim                             # XMPP Client
    jitsi-meet                        # Video Calls & Chat
    mumble                            # Low-Latency, High Quality Voice Chat
    vesktop                           # Discord Client with Vencord
    zoom                              # Video Calls & Chat

  # Cursors
    phinger-cursors                   # Cursor Theme

  # DAW
    ardour                            # DAW
    audacity                          # Lightweight DAW
    audiowaveform                     # Convert Audio Files & can export Visual Images of Them
    #bitdragon
    bitwig-studio                     # DAW
    #cakewalk
    carla                             # Audio Plugin Host
    #cecilia
    lmms                              # DAW
    #mixbus
    mixxx                             # DJ Mixing Software
    muse                              # MIDI/Audio Sequencer 
    #ossia                            # DAW
    qtractor                          # MIDI/Audio Sequencer
    #radium
    reaper                            # DAW
    #score
    #stargate
    tenacity                          # Lightweight DAW
    renoise                           # Tracker-based DAW
    rosegarden                        # Composition & Editing Environment 
    #traktion waveform
    yabridge                          # Use Windows VST2 & VST3 Plugins on Linux
    zrythm                            # DAW

  # Games
    airshipper                        # Veloren RPG Updater
    assaultcube                       # First-Person Shooter
    alienarena                        # First-Person Shooter
    cataclysm-dda                     # Zombie Rogue-Like
    clairvoyant                       # Clairvoyant Magic 8 Ball
    factorio                          # Build & Maintain Factories
    flightgear                        # Flight Simulator
    freeciv                           # Civilization Strategy Clone
    freenukum                         # Duke Nukum 1 Clone
    gcompris                          # Educational Suite for Children age 2-10
    gzdoom                            # Doom Clone based on ZDoom
    hedgewars                         # Worms Clone
    keeperrl                          # Dungeon Rogue-Like
    openttd                           # Transport Tycoon Deluxe Clone
    openrct2                          # RollerCoaster Tycoon 2
    redeclipse                        # First-Person Shooter
    rili                              # Children's Train Game
    sauerbraten                       # First-Person Shooter, successor of Cube
    scorched3d                        # Scorched Earth 3D Clone
    shattered-pixel-dungeon           # Roguelike Dungeon Crawl
    simutrans                         # Transportation Simulator
    space-cadet-pinball               # Space Cadet Pinball Game
    supertux                          # Super Mario Clone
    supertuxkart                      # Super Mario Kart Clone
    tuxpaint                          # Drawing for Children
    warzone2100                       # Real-Time Strategy
    warsow                            # First-Person Shooter
    wesnoth                           # Fantasy Strategy
    ufoai                             # X-Com Clone
    unciv                             # Civilization Strategy Clone
    xonotic                           # First-Person Shooter
    zdoom                             # Doom Clone
    zeroad                            # Ancient Warfare Strategy

  # Gaming
    gamemode                          # Optimse Linux System Performance
    cdecrypt                          # Wii U Emulator 
    cemu                              # Wii U Emulator
    dolphin-emu                       # Wii and GameCube Emulator
    heroic                            # GOG & Epic Games Launcher
                                        # NOTE: Issues with authenticating, package out of date, using Flatpak
    itch                              # itch.io Games Launcher
    legendary-gl                      # Epic Games Launcher Alternative
    ludusavi                          # Backup tool for PC game saves
    lutris                            # Issues with Origin installation,
                                        # using Flatpak
    mangohud                          # Gaming Stat HUD
    mumble                            # Low-latency, high quality voice chat software
    mupen64plus                       # N64 Emulator
    PPSSPP                            # PSP Emulator
    RPCS3                             # PS3 Emulator
    retroarchFull                     # Multi-Platform Emulator
    ryujinx                           # Switch Emulator
    steam                             # Steam Game Launcher
    vita3K                            # PSVita Emulator

  # Todo:
    #zero ballistics                  # ??
    #the dark mod                     # ??

  # Gnome
    adw-gtk3                          # GTK3 libadwaita Theme ported to GTK-3
    dconf                             # Gnome Configuration
    dconf2nix                         # Nixify Gnome Config File    
    gnome.gnome-tweaks                # Gnome Customization Tool  

  # Hyprland
    dunst                             # Notification Daemon
    eww                               # ElKowars Wacky Widgets
    grim                              # Wayland Image Grab
    hyprpaper                         # Wallpaper Application for Wayland Compositors   

  # Internet
    betterbird                        # Email Client, No pkg,
                                        # using flatpak
    brave                             # Browser
                                        # w/ BAT, using flatpak
    firefox                           # Web Browser
    librewolf                         # Firefox Web Browser Fork
    thunderbird                       # Email Client
    tigervnc                          # VNC Client
    tor-browser-bundle-bin            # Privacy Web Browser
  
  # Media
    annotator                        # Elementary Image Annotator
    cipher                           # Elementary Text Encryptor
    clapper                          # Media Player
    hashit                           # Elementary Checksum Calculator
    ideogram                         # Elementary Emoji Selector
    minder                           # Elementary Mind Mapper
    nasc                             # Elementary Mathmatical Calculator
    bibletime                        # Bible Study Tool
    blender                          # 3D Animation/Publishing
    bookworm                         # eBook Reader
    calibre                          # eBook Library
    converseen                       # Batch Image Converter
    davinci-resolve                  # Professional Video Editing, Color, Effects and Audio Post
    digikam                          # Photo Management Program
    easytag                          # Music Tag Editor
    feh                              # Image Viewer
    foliate                          # eBook Reader
    fooyin                           # Foobar Music Player Clone
    freetube                         # YouTube Front-End
    ffmpeg_5-full                    # Record, Convert and Stream Audio and Video
    gimp                             # GNU Image Manipulation Program
    gpodder                          # Podcast Manager
    handbrake                        # Video Conversion Tool
    hypnotix                         # IPTV streaming application
    inkscape                         # Vector Graphics Editor
    koodo-reader                     # eBook Reader
    libsForQt5.kolourpaint           # Painting Application
    llpp                             # PDF Viewer
    jellyfin-media-player            # Jellyfin Desktop Client based on Plex Media Player
    krita                            # Painting Application
    libsForQt5.k3b                   # Disk Burning Application
    makemkv                          # Blu-Ray and DVD to MKV Converter
    mpv                              # Media Player
    mupdf                            # PDF Viewer           
    noisetorch                       # Microphone Noise Supression for PulseAudio
    nomacs                           # Qt-based image viewer
    nuclear                          # Music Streamer
    ocrmypdf                         # OCR Text Layer to Scanned PDFs
    obs-studio                       # Recording/Streaming Application
    libsForQt5.okular                # Document Viewer
    qview                            # Minimal Image Viewer
    scrcpy                           # Display and control Android devices over USB or TCP/IP
    shortwave                        # Internet Radio
    shutter encoder                  # A/V Converter; Use appimage
    sigil                            # ePub Editor
      pageedit                       # ePub XHTML Visual Editor
    soundconverter                   # GTK Audio file converter
    smile                            # Emoji Picker
    smplayer                         # Media Player
    strawberry                       # Media Player
    stremio                          # Media Streaming Center
    xiphos                           # Bible Study Tool
    zathura                          # PDF Viewer
    zotero                           # Research Sourcer

  # Office
    adobe-reader                     # PDF Reader (for government forms mainly)
    ascii-draw                       # Diagrams using ASCII
    beekeeper-studio                 # SQL client for MySQL, Postgres, SQLite, SQL Server, and more
    ferdium                          # Communication & Services Manager    
    gImageReader                     # Tesseract-OCR Front-End
    gnome-frog                       # Text extraction tool (OCR)
    gnucash                          # Double Entry Accounting Software
                                        # Gnucash requires dconf, see Gnome section above
    libreoffice                      # Office Suite (Stable)
    libreoffice-qt                   # Office Suite (for KDE)
    libreoffice-fresh                # Office Suite (Latest)
      hunspell                       # Libreoffice Spell Check 
      hunspellDicts.en_US            # hunspell English 
    obsidian                         # Markdown Editor
    onlyoffice-bin                   # Office Suite
    #openbb                          # Investment Research Tool; NOTE: currently using through docker
    pdfarranger                      # PDF Arranger
    pdf4qt                           # Open source PDF editor
    portfolio                        # Portfolio Performance 
    qpdfview                         # Tabbed PDF Viewer
    xournalpp                        # PDF Annotation

  # Peripherals
    hplip                            # HP Print Drivers

  # Science
    celestia                         # Space Simulation
    freecad                          # Open Source 3D CAD
    gramps                           # Genealogy Software
    gretl                            # Econometric Analysis
    labplot                          # Plotting Software (unavailable- use flatpak)
    rstudioWrapper                   # Integrated Tools for the R Language
    speedcrunch                      # Calculator
    stellarium                       # Free open-source planetarium

  # System
    alacritty                        # Terminal Emulator
    appimage-run                     # Appimage Runner
    bleachbit                        # PC Cleaner
    borgbackup                       # Deduplicating Backup Archiver
    bottles                          # Wineprefix Manager
    cmatrix                          # Matrix Movie Terminal Theme
    coolercontrol                    # Fan Monitor
    czkawka                          # Duplicate file finder
    btop                             # Resource Monitor
    cpu-x                            # PC Information
    crow-translate                   # Language Translator
    dupeguru                         # Duplicate file finder 
    etcher                           # Flash OS images to SD cards and USB drives, safely and easily
    gcc                              # GNU Compiler Collection
    gcolor3                          # RGB Value Selector
    genymotion                       # NOTE: Issues with loading
    git                              # Distributed Version Control System
    gocryptfs                        # Encrypted Overlay Filesystem   
      cryptor                          # gocryptfs gui
      sirikali                         # gocryptfs gui
    gpt4all                          # Private AI Chatbot Manager
    gsmartcontrol                    # Disk Health Inspection Tool
    htop                             # Process Viewer
    i2c-tools                        # Network Utilities
    inetutils                        # Network Utilities
    input-leap                       # KVM Software
    keepassxc                        # GUI Password Manager with Cryptography
    kitty                            # Terminal Emulator
    kitty-themes                     # Kitty Themes
    lan-mouse                        # KVM Software (unencrypted)
    localsend                        # Send Files/Text Over Wifi
    openrgb                          # RGB Manager
    mission-center                   # System Monitor
    neofetch                         # System Information
    nitch                            # Nim System Fetch
    nix-software-center              # Nix Software Center
    #nomachine-client                 # NoMachine remote desktop client
    normcap                          # NormCap Screen OCR Tool
    nox                              # Nix Tools
    ntfs3g                           # FUSE Client
    p7zip                            # File Archiver
    papirus-icon-theme               # Icons
    powertop                         # Power Manager
    protonup                         # Proton Installer
    qbittorrent                      # Torrent Application
    qemu_full                        # Virtual Machine
    resources                        # System Resource Monitor
    rustdesk                         # Remote Desktop Software
    simplescreenrecorder             # Screen Recorder
    solaar                           # Logitech Unifying Receiver
    speedcrunch                      # Calculator
    syncthing                        # File Synchronization
    syncthing-tray                   # Tray for Syncthing
    tixati                           # Torrent Application (removed from nixpks, unfree, no longer maintained)
    tldr                             # Simplified man pages
    waydroid                         # Android Emulator
                                        # Only works on wayland
    warp                             # Secure File Transfer
    ventoy                           # USB Boot App
    veracrypt                        # Filesystem Encryption
    virtualbox                       # Virtual Machine
                                        # Only needed with genymotion
    vorta                            # borgbackup gui
    vscodium                         # Code Editor
    xorg.xkill                       # Kill Windows w/ Mouse
    xpipe                            # A cross-platform shell connection hub and remote file manager
    xscreensaver                     # Screensavers

  # Terminal/CLI Related
    asciiquarium                     # Terminal Aquarium
    bash-completion                  # Bash Autocomplete
    bat                              # cat like concatenate or display command
    bottom                           # System Monitor
    broot                            # Directory Navigator
    btop                             # Resource Monitor
    cava                             # Console-based Audio Visualizer for Alsa
    carapace                         # Argument Completer
    du-dust                          # du command replacement
    nix-bash-completions             # Bash Autocomplete
    cmatrix                          # Matrix Digital Rain
    cbonsai                          # Terminal Bonsai Tree
    delta                            # syntax-highlighting pager for git
    difftastic                       # diff replacement
    eza                              # ls like list command
    fd                               # find command
    fzf                              # Fuzzy Finder in GO
    genact                           # Nonsense Activity Generator
    hollywood                        # Hollywood Melodrama Technobabble
    jq                               # filter data command
    kitty                            # Terminal
    kitty-themes                     # Kitty Terminal Themes
    lavat                            # Terminal Lavalamp
    monolith                         # Single HTML Saver
    neo                              # Matrix Digital Rain
    nix-bash-completions             # Bash Autocomplete
    nitch                            # Nim System Fetch
    nms                              # Effect from 1992 Movie Sneakers
    nyancat                          # Terminal Nyancat
    ouch                             # Obvious Unified Compression Helper
    pandoc                           # Markup Converter
    pipes                            # Animated Pipes Terminal Screensaver
    procs                            # Processes, `ps` replacement
    ranger                           # File Manager
    ripgrep-all                      # search command
    rofi                             # Wayland Launcher/Menu
    sd                               # find & replace command
    shell-genie                      # Natural Language Shell Command AI
    skim                             # Fuzzy Finder
    sl                               # Terminal Locomotive
    slurp                            # Select a Region on the Wayland Compositor
    superfile                        # Modern Terminal File Manager
    tealdeer                         # tldr command
    tokei                            # Code Statistics
    waybar                           # Wayland Bar for Wlroots Based Compositors
    wbg                              # Wallpaper Application for Wayland Compositors
    wofi                             # Wayland Launcher/Menu 
    xcowsay                          # Graphical Cowsay  
    zoxide                           # fast cd directory command
}
