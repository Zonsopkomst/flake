---
title: Adobe
type: Instructions
author: Zonsopkomst
created: 2023
---
# Adobe Instructions
[[adobereader]] [[bottles]] [[wishlist]] [[taxes]] [[2023]]

[Adobe Acrobat Bottles Reddit](https://libreddit.garudalinux.org/r/Ubuntu/comments/uj2u61/adobe_acrobat_dc_on_bottles/)

## u/M4a1x Jan 26 '23
Just got it to work, since MasterPDF couldn't handle the form I was trying to fill out. Here is a detailed description of the steps, maybe it helps someone. Automatic updates and protected mode don't work though.

I downloaded the current Adobe Reader 11 (Select Windows XP).

Made a bottle with Environment "Application"

Under Dependencies add:
atmlib
riched20
mspatcha

Run executable: Select the downloaded installer. It's gonna fail! Don't click "Finish"! This would remove the installation again. It fails as it can't set some system settings..

Open a Terminal and go to the bottle installation to backup the folder (I have the flatpak version, you install location might differ): bash cd $HOME/.var/app/com.usebottles.bottles/data/bottles/bottles/Adobe-Reader/drive_c/Program Files (x86)/Adobe/Reader 11.0 cp -R Reader 11.0/ Reader 11.0-backup

Click Finish now

Move the backup back to where it was (and delete the leftovers before, in the same termial): bash rm -r Reader 11.0 mv Reader 11.0-backup/ Reader 11.0

Now manually add the Shortcut in bottles through Add Shortcuts... in bottles. You're looking for the file $HOME/.var/app/com.usebottles.bottles/data/bottles/bottles/Adobe-Reader/drive_c/Program Files (x86)/Adobe/Reader 11.0/Reader/AcroRd32.exe. The .var folder is hidden by default in the File Chooser, Press Ctrl+H to make it visible

Launch Acrobat through Bottle!

Inspired by: [This Quora Post](https://www.quora.com/How-do-I-install-Adobe-Reader-on-Ubuntu-using-the-Wine-software) and this [WineHQ Entry](https://appdb.winehq.org/objectManager.php?sClass=version&iId=32266)