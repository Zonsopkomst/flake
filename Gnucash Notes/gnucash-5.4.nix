# # First Attempt
#   # Did not record
# 
# # Second Attempt (as gnucash-5.4.nix file, import in home-manager.nix)
# { pkgs ? import <nixpkgs> {} }:
# 
# pkgs.haskellPackages.override {
#   gnucash = pkgs.gnucash.override {
#     version = "5.4";
#   };
# }
# 
# # Third Attempt (as gnucash-5.4.nix file, import in home-manager.nix)
# (pkgs.gnucash.overrideAttrs (oldAttrs: {
#   version = "5.4";
# 
#   src = oldAttrs.src.overrideAttrs {
#     outputHash = lib.fakeHash;
#   };
# }))
# 
# # Fourth Attempt (as gnucash-5.4.nix file, import in home-manager.nix)
# { pkgs, ... }:
# 
# let
#   old-pkgs-src = builtins.fetchurl "https://github.com/nixos/archive/nixos-22.03.tar.gz";
#   old-pkgs = import old-pkgs-src;
#   overlay = self: super: {
#     gnucash = old-pkgs.gnucash;
#   };
# in {
#   nixpkgs.overlays = [ overlay ];
# }
# 
# # Fifth Attempt (as gnucash-5.4.nix file, import in home-manager.nix)
# { nixpkgs, home-manager ? import <nixpkgs> {}, ... }:
# 
# let
#   gnucash5.4 = nixpkgs.overlays.buildOverlay.apply(nixpkgs) {
#     gnucash = nixpkgs.overlays.pinVersion {
#       pkg = nixpkgs.gnucash;
#       version = "5.4";
#     };
#   };
# in
# {
#   home-manager.overrides = home-manager.lib.overridePkgs home-manager {
#     gnucash = gnucash5.4.gnucash;
#   };
# }
# 
# # Didn't try yet (add to home-mangaer.nix directly?  Remove gnucash package first?)
# home.services.gnucash = {
#   enable = true;
#   package = gnucash5.4;
# };
# 
# # OR??
# {config, ... }:
# let
#   gnucash_fix = import (
#     fetchTarball https://github.com/NixOS/nixpkgs/archive/<REV>.tar.gz
#     ){ { inherit (config.nixpkgs) system config; } };
# in
# {
# environment.systemPackages = [
# gnucash_fix.gnucash
# ];
# }

# Last attempt
# https://github.com/NixOS/nixpkgs/commit/f4599feae4160285c5ce4e4364bd147df83e780c
{
  gnucash = super.gnucash.overrideAttrs (old: rec {
    name = "gnucash-${version}";
    version = "5.4";
    src = super.requireFile {
      name = "https://github.com/Gnucash/gnucash/releases/download/${version}/${pname}-${version}-1.tar.bz2";
      sha256 = "sha256-d0EWXW1lLqe0oehJjPQ5pWuBpcyLZTKRpZBU8jYqv8w=";
    };
  });
}

# Latest Attempt:
(gnucash.overrideAttrs (old: rec {
       name = "gnucash-${version}";
       version = "5.4";
       src = requireFile {
         name = "https://github.com/Gnucash/gnucash/releases/download/${version}/${pname}-${version}-1.tar.bz2";
         #sha256 = "0000000000000000000000000000000000000000000000000000";
         sha256 = "sha256-d0EWXW1lLqe0oehJjPQ5pWuBpcyLZTKRpZBU8jYqv8w=";
        };
      }))