{ config, pkgs, ... }:  

{
  home-manager.users.leeuwarden = { pkgs, ... }: {
    home.packages = with pkgs; [
     annotator               # Elementary Image Annotator
     armcord                 # Discord Client  
     ardour                  # DAW
     ascii-draw              # Diagrams using ASCII
     borgbackup              # Deduplicating Backup
     brave                   # Browser
     calibre                 # eBook Library
     cdecrypt                # Wii U Emulator 
     cemu                    # Wii U Emulator
     cipher                  # Elementary Text Encryptor
     clairvoyant             # Clairvoyant Magic 8 Ball 
     #coolercontrol           # Fan Monitor
     crow-translate          # Language Translator
     czkawka                 # Duplicate file finder
     dino                    # XMPP Client
     dolphin-emu             # Wii and GameCube Emulator 
     dupeguru                # Duplicate file finder 
     easytag                 # Music Tag Editor
     element-desktop         # Matrix Client
     emote                   # Emoji Selector
     freetube                # YouTube Front-End
     fooyin                  # Foobar Music Player Clone
     gcolor3                 # RGB Value Selector
     gimp                    # GNU Image Manipulation Program
     gnucash                 # Double Entry Accounting Software
     #gpodder                 # Podcast Manager
     gpt4all                 # Private AI Chatbot Manager
     hashit                  # Elementary Checksum Calculator
     inkscape                # Vector Graphics Editor
     #itch                    # itch.io Games Launcher (NOTE: Also check flake for inputs.itchpkgs.url)
     keepassxc               # GUI Password Manager with Cryptography
     kitty                   # Terminal
     kitty-themes            # Kitty Terminal Themes
     koodo-reader            # eBook Reader
     krita                   # Painting Application
     lan-mouse               # KVM Software (unencrypted)
     librewolf               # Firefox Web Browser Fork
     libsForQt5.kolourpaint  # Painting Application
     llpp                    # PDF Viewer
     #localsend               # Send Files/Text Over Wifi
     ludusavi                # Backup tool for PC game saves 
     makemkv                 # Blu-Ray and DVD to MKV Converter
     mangohud                # Gaming Stat HUD
     mission-center          # System Monitor
     mpv                     # Media Player
     mumble                  # Low-Latency, High Quality Voice Chat
     mupdf                   # PDF Viewer           
     nasc                    # Elementary Mathmatical Calculator
     nomacs                  # Qt-based image viewer
     #normcap                 # NormCap Screen OCR Tool
     nuclear                 # Music Streamer
     obs-studio              # Recording/Streaming Application
     onlyoffice-bin          # Office Suite
     openrgb                 # RGB Manager
     p7zip                   # File Archiver
     pdfarranger             # PDF Arranger
     python3                 # python langugage
     qbittorrent             # Torrent Application
     retroarchFull           # Multi-Platform Emulator
     resources               # System Resource Monitor    
     #rustdesk                # Remote Desktop Software   
     shattered-pixel-dungeon # Roguelike Dungeon Crawl
     smplayer                # Media Player
     solaar                  # Logitech Unifying Receiver
     soundconverter          # GTK Audio file converter
     space-cadet-pinball     # Space Cadet Pinball Game
     speedcrunch             # Calculator
     superfile               # Modern Terminal File Manager
     tenacity                # Sound Editor
     tor-browser-bundle-bin  # Privacy Web Browser
     ventoy                  # USB Boot App
     veracrypt               # Filesystem Encryption    
     vorta                   # borgbackup gui
     vscodium                # Code Editor  
     xiphos                  # Bible Study Tool
     xournalpp               # PDF Annotation
     xscreensaver            # Screensavers
     zathura                 # PDF Viewer
    ];

    imports = [ 
      ../../config/config.nix
      ../../common/emacs.nix
      ../../common/git.nix
      ../../common/vscodium.nix 
    ];

    home.stateVersion = "22.05"; 
  };
}