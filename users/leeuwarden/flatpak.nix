# Added 10/25
{ config, pkgs, ... }:  

{
  services.flatpak.packages = [
  #  { appId = "com.brave.Browser"; origin = "flathub";  } or as follows:
  "io.github.Foldex.AdwSteamGtk"         # ADW Steam GTK Skin
  "eu.betterbird.Betterbird"             # Betterbird Email Client
  "io.github.giantpinkrobots.flatsweep"  # flatsweep                
  "com.github.tchx84.Flatseal"           # Flatseal Flatpak Manager 
  "org.gtk.Gtk3theme.adw-gtk3"           # Gnome Theme
  "org.gtk.Gtk3theme.adw-gtk3-dark"      # Gnome Theme
  "org.localsend.localsend_app"          # File/Message Send
  "com.github.dynobo.normcap"            # NormCap 
  "com.heroicgameslauncher.hgl"          # Heroic Games Launcher
  #"org.libretro.RetroArch"               # RetroArch
  "com.github.zocker_160.SyncThingy"     # SyncThingy   
  "io.github.flattool.Warehouse"         # Flatpak Warehouse          
  ];

    # This doesn't seem to solve the `Failed to start flatpak-managed-install.service.` issue:
    systemd.services."flatpak-managed-install" = {
    serviceConfig = {
      ExecStartPre = "${pkgs.coreutils}/bin/sleep 5";
    };
  };
}
