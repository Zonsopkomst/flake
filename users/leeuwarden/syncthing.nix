{ config, pkgs, ... }:  

{
  services = {
      syncthing = {
          enable = true;
          user = "leeuwarden";
          dataDir = "/home/leeuwarden/Sync";    # Default folder for new synced folders
          configDir = "/home/leeuwarden/Documents/.config/syncthing";   # Folder for Syncthing's settings and keys
      };
  };
}