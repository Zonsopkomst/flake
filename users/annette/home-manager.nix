{ config, pkgs, ... }:  

{
  home-manager.users.annette = { pkgs, ... }: {
    home.packages = with pkgs; [
      borgbackup                # Deduplicating archiver with compression and encryption
      bleachbit                 # PC Cleaner
      brave                     # Browser
      easytag                   # Music Tag Editor
      firefox                   # Browser
      libsForQt5.kolourpaint    # Painting Application
      kitty                     # Terminal Emulator
      mpv                       # Media Player
      #nomachine-client          # NoMachine remote desktop client (nxplayer)
      onlyoffice-bin            # Office Suite
      retroarchFull             # Multi-Platform Emulator
      smplayer                  # Media Player
      speedcrunch               # Calculator
      strawberry                # Media Player
      vorta                     # borgbackup gui
      vscodium                  # Code Editor
    ];

    imports = [ 
      ../../config/config.nix
      ../../common/git.nix
      ../../common/vscodium.nix 
    ];

     home.stateVersion = "23.05";
  };
}
