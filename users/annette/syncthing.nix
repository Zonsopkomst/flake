{
  services = {
      syncthing = {
          enable = true;
          user = "annette";
          dataDir = "/home/annette/Sync";    # Default folder for new synced folders
          configDir = "/home/annette/Documents/.config/syncthing";   # Folder for Syncthing's settings and keys
      };
  };
}