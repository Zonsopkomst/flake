{
  services = {
      syncthing = {
          enable = true;
          user = "coleman";
          dataDir = "/home/coleman/Sync";    # Default folder for new synced folders
          configDir = "/home/coleman/Documents/.config/syncthing";   # Folder for Syncthing's settings and keys
      };
  };
}
