{ config, pkgs, ... }:  

{
  home-manager.users.coleman = { pkgs, ... }: {
    home.packages = with pkgs; [
      bleachbit                 # PC Cleaner
      borgbackup                # Deduplicating archiver with compression and encryption
      brave                     # Brave Browser
      calibre                   # eBook Library
      crow-translate            # Language Translator
      czkawka                   # Duplicate file finder
      dino                      # XMPP Client
      dupeguru                  # Duplicate file finder 
      easytag                   # Music Tag Editor
      emote                     # Emoji Selector
      freetube                  # YouTube Front-End
      gcolor3                   # RGB Value Selector
      #gpodder                   # Podcast Manager
      keepassxc                 # GUI Password Manager with Cryptography
      kitty                     # Terminal Emulator
      koodo-reader              # eBook Reader
      librewolf                 # Firefox Web Browser Fork
      libsForQt5.kolourpaint    # Painting Application
      #localsend                 # Send Files/Text Over Wifi
      mission-center            # System Monitor
      mpv                       # Media Player
      mumble                    # Low-Latency, High Quality Voice Chat
      nomacs                    # Qt-based image viewer
      #normcap                   # NormCap Screen OCR Tool
      onlyoffice-bin            # Office Suite
      p7zip                     # File Archiver
      pdfarranger               # PDF Arranger
      smplayer                  # Media Player
      solaar                    # Logitech Unifying Receiver
      speedcrunch               # Calculator
      strawberry                # Media Player
      resources                 # System Resource Monitor
      #retroarchFull             # Multi-Platform Emulator
      rustdesk                  # Remote Desktop Software
      vorta                     # borgbackup gui
      vscodium                  # Code Editor
      xiphos                    # Bible Study Tool
      xournalpp                 # PDF Annotation
    ];

    imports = [ 
      ../../config/config.nix
      ../../common/git.nix
      ../../common/vscodium.nix 
    ];

     home.stateVersion = "23.05";
  };
}
