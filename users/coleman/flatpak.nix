{ config, pkgs, ... }:  

{
  services.flatpak.packages = [
    "io.github.giantpinkrobots.flatsweep"  # flatsweep                
    "com.github.tchx84.Flatseal"           # Flatseal Flatpak Manager 
    "org.gtk.Gtk3theme.adw-gtk3"           # Gnome Theme
    "org.gtk.Gtk3theme.adw-gtk3-dark"      # Gnome Theme
    "org.localsend.localsend_app"          # File/Message Send
    "com.github.dynobo.normcap"            # NormCap 
    "com.github.zocker_160.SyncThingy"     # SyncThingy   
    "io.github.flattool.Warehouse"         # Flatpak Warehouse        
  ];
}