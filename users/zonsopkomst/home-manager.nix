{ config, pkgs, ... }:  

{
  home-manager.users.zonsopkomst = { pkgs, ... }: {

    home.packages = with pkgs; [
     annotator                        # Elementary Image Annotator
     armcord                          # Discord Client        
     ascii-draw                       # Diagrams using ASCII
     bleachbit                        # PC Cleaner
     borgbackup                       # Deduplicating Backup Archiver
     brave                            # Brave Browser
     calibre                          # eBook Library
     cipher                           # Elementary Text Encryptor
     clairvoyant                      # Clairvoyant Magic 8 Ball
     clapper                          # Media Player
     colorpicker                      # RGB Value Selector
     converseen                       # Batch Image Converter
     #coolercontrol                    # Fan Monitor
     crow-translate                   # Language Translator
     czkawka                          # Duplicate file finder
     dconf2nix                        # Nixify Gnome Config File            
     dino                             # XMPP Client
     dupeguru                         # Duplicate file finder 
     element-desktop                  # Matrix Client
     easytag                          # Music Tag Editor
     emote                            # Emoji Selector
     freetube                         # YouTube Front-End
     fooyin                           # Foobar Music Player Clone
     gajim                            # XMPP Client
     gnucash                          # Double Entry Accounting Software
     gocryptfs                        # Encrypted Overlay Filesystem   
       sirikali                         # gocryptfs gui
     #gpodder                          # Podcast Manager   
     gretl                            # Econometric Analysis
     gzdoom                           # Doom Clone based on ZDoom
     hashit                           # Elementary Checksum Calculator
     josm                             # Extensible editor for OpenStreetMap  
     keepassxc                        # GUI Password Manager with Cryptography
     kitty                            # Terminal Emulator
     kitty-themes                     # Kitty Themes
     koodo-reader                     # eBook Reader
     lan-mouse                        # KVM Software (unencrypted)
     librewolf                        # Firefox Web Browser Fork
     libsForQt5.kolourpaint           # Painting Application
     libsForQt5.k3b                   # Disk Burning Application
     #localsend                        # Send Files/Text Over Wifi
     llpp                             # PDF Viewer
     mission-center                   # System Monitor
     mpv                              # Media Player 
     mupdf                            # PDF Viewer           
     nasc                             # Elementary Mathmatical Calculator   
     #nomachine-client                 # NoMachine remote desktop client
     nomacs                           # Qt-based image viewer
     #normcap                          # NormCap Screen OCR Tool
     nuclear                          # Music Streamer
     onlyoffice-bin                   # Office Suite
     p7zip                            # File Archiver
     pdfarranger                      # PDF Arranger
     portfolio                        # Portfolio Performance 
     python3                          # python langugage
     qbittorrent                      # Torrent Application
     qgis                             # A Free and Open Source Geographic Information System
     #retroarchFull                    # Multi-Platform Emulator
     resources                        # System Resource Monitor
     #rustdesk                         # Remote Desktop Software
     rstudioWrapper                   # Integrated Tools for the R Language
     smplayer                         # Media Player
     solaar                           # Logitech Unifying Receiver
     soundconverter                   # GTK Audio file converter
     space-cadet-pinball              # Space Cadet Pinball Game
     strawberry                       # Media Player            
     speedcrunch                      # Calculator
     superfile                        # Modern Terminal File Manager
     thunderbird                      # Thunderbird Email Client
     tor-browser-bundle-bin           # Privacy Web Browser
     ventoy                           # USB Boot App
     veracrypt                        # Filesystem Encryption
     vesktop                          # Discord Client with Vencord
     vorta                            # borgbackup gui
     xiphos                           # Bible Study Tool
     xournalpp                        # PDF Annotation
     xscreensaver                     # Screensavers
     zathura                          # PDF Viewer
    ];

    imports = [ 
      ../../config/config.nix
      ../../common/emacs.nix
      ../../common/git.nix
      ../../common/vscodium.nix
    ];

     home.stateVersion = "22.11";
  };
}
