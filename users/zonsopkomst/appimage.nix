    appimageTools.wrapType2 { # or wrapType1
      name = "colorpicker";
      src = fetchurl {
        url = "https://github.com/Toinane/colorpicker/releases/download/2.2.0/Colorpicker-2.2.0.AppImage"
        hash = "0000000000000000000000000000000000000000000000000000";
      };
      extraPkgs = pkgs: with pkgs; [ ];
    }