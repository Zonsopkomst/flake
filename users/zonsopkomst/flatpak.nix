{ config, pkgs, ... }:  

{
  services.flatpak.packages = [
  #  { appId = "com.brave.Browser"; origin = "flathub";  }
  "io.github.Bavarder.Bavarder"          # Bavarder   
  "io.github.giantpinkrobots.flatsweep"  # flatsweep                
  "com.github.tchx84.Flatseal"           # Flatseal Flatpak Manager 
  "flathub it.mijorus.gearlever"         # Gear Lever AppImage Manager
  "org.gtk.Gtk3theme.adw-gtk3"           # Gnome Theme
  "org.gtk.Gtk3theme.adw-gtk3-dark"      # Gnome Theme
  "org.localsend.localsend_app"          # File/Message Send
  "flathub io.github.subhra74.Muon"      # Muon SSH Client
  "com.github.dynobo.normcap"            # NormCap Screen OCR Tool 
  #"org.libretro.RetroArch"               # RetroArch
  "com.github.zocker_160.SyncThingy"     # SyncThingy   
  "io.github.flattool.Warehouse"         # Flatpak Warehouse        
  ];
}