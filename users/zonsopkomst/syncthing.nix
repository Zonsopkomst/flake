{
  services = {
      syncthing = {
          enable = true;
          user = "zonsopkomst";
          dataDir = "/home/zonsopkomst/Sync";    # Default folder for new synced folders
          configDir = "/home/zonsopkomst/Documents/.config/syncthing";   # Folder for Syncthing's settings and keys
      };
  };
}