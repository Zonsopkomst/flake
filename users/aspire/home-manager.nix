{ config, pkgs, ... }:  

{
  home-manager.users.aspire = { pkgs, ... }: {
     home.packages = with pkgs; [
      borgbackup                       # Deduplicating archiver with compression and encryption
      czkawka                          # Duplicate file finder
      dupeguru                         # Duplicate file finder
      imagemagick                      # Imagemagick 
      imagination                      # Imagination
      kitty                            # Terminal Emulator
      kitty-themes                     # Kitty Themes
      librewolf                        # Firefox Web Browser Fork
      tor-browser-bundle-bin           # Privacy Web Browser
      nomacs                           # Qt-based image viewer
      mpv                              # Media Player
      veracrypt                        # Filesystem Encryption    
      vorta                            # borgbackup gui
     ];

    imports = [ 
      ../../config/config.nix
      ../../common/git.nix
      ../../common/vscodium.nix 
    ];
    
     home.stateVersion = "23.05"; 
  };
}
