{ config, pkgs, ... }:

{
fileSystems."/data" =
  { device = "/dev/disk/by-uuid/72666366-e2b9-46d2-a2c3-a2931ba72566";
    fsType = "ext4";
  };
}