{
  services = {
      syncthing = {
          enable = true;
          user = "admin";
          dataDir = "/home/admin/Sync";    # Default folder for new synced folders
          configDir = "/home/admin/Documents/.config/syncthing";   # Folder for Syncthing's settings and keys
      };
  };
}