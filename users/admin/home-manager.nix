{ config, pkgs, ... }:  

{
  home-manager.users.admin = { pkgs, ... }: {
    home.packages = with pkgs; [
      borgbackup                # Deduplicating archiver with compression and encryption
      bleachbit                 # PC Cleaner
      bristol                   # A range of synthesiser, electric piano and organ emulations
      brave                     # Brave Browser
      calibre                   # eBook Library
      crow-translate            # Language Translator
      dino                      # XMPP Client
      easytag                   # Music Tag Editor
      emote                     # Emoji Selector
      librewolf                 # Firefox Web Browser Fork
      freetube                  # YouTube Front-End
      gcolor3                   # RGB Value Selector
      gcompris                  # Educational Suite for Children age 2-10
      #gpodder                   # Podcast Manager
      koodo-reader              # eBook Reader
      libsForQt5.kolourpaint    # Painting Application
      #localsend                 # Send Files/Text Over Wifi
      keepassxc                 # GUI Password Manager with Cryptography
      kitty                     # Terminal Emulator
      mission-center            # System Monitor
      mpv                       # Media Player
      mumble                    # Low-Latency, High Quality Voice Chat
      nomacs                    # Qt-based image viewer
      #normcap                   # NormCap Screen OCR Tool
      nuclear                   # Music Streamer
      onlyoffice-bin            # Office Suite
      pdfarranger               # PDF Arranger
      pianobooster              # A MIDI file player that teaches you how to play the piano
      qsynth                    # Fluidsynth GUI 
      resources                 # System Resource Monitor
      #retroarchFull             # Multi-Platform Emulator
      rili                      # Children's Train Game
      #rustdesk                  # Remote Desktop Software
      smplayer                  # Media Player
      solaar                    # Logitech Unifying Receiver
      speedcrunch               # Calculator
      strawberry                # Media Player
      synthesia                 # A fun way to learn how to play the piano
      veracrypt                 # Filesystem Encryption
      vorta                     # borgbackup gui
      vscodium                  # Code Editor
      xiphos                    # Bible Study Tool
      xournalpp                 # PDF Annotation
      vmpk                      # Virtual MIDI Piano Keyboard
    ];

    imports = [ 
      ../../config/config.nix
      ../../common/git.nix
      ../../common/vscodium.nix 
    ];

     home.stateVersion = "22.11";
  };
}
