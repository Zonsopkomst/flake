{ config, pkgs, ... }:

{
  imports = [ 
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  boot.initrd.luks.devices."luks-b0ef797e-169e-4930-b687-98322f94f92d".device = "/dev/disk/by-uuid/b0ef797e-169e-4930-b687-98322f94f92d";
  boot.initrd.luks.devices."luks-b0ef797e-169e-4930-b687-98322f94f92d".keyFile = "/crypto_keyfile.bin";

  networking.hostName = "weda"; # Define your hostname.

  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;

  users.users.zonsopkomst = {
    isNormalUser = true;
    description = "Zonsopkomst";
    extraGroups = [ "docker" "networkmanager" "wheel" ];
    packages = with pkgs; [
    ];
  };
  
  # Added 07/23/2024 in attempt to ignore locking the screen when laptop is closed
  services.logind.extraConfig = "HandleLidSwitch=ignore";

  environment.systemPackages = with pkgs; [
  ];

  system.stateVersion = "22.11";

}
