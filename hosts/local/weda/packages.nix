{ config, pkgs, ... }:

{
  imports = [ ../../../common/fonts.nix];

  programs.hyprland.enable = true;
        
  environment.systemPackages = with pkgs; [
    adw-gtk3                         # GTK3 libadwaita Theme ported to GTK-3
    cpu-x                            # PC Information
    dconf                            # Gnome Configuration
    gcc                              # GNU Compiler Collection
    gramps                           # Genealogy Software     
    gsmartcontrol                    # Disk Health Inspection Tool
    i2c-tools                        # Network Utilities
    inetutils                        # Network Utilities
    nox                              # Nix Tools
    ntfs3g                           # FUSE Client
    papirus-icon-theme               # Icons
    phinger-cursors                  # Cursor Theme
    powertop                         # Power Manager
    protonup                         # Proton Installer
    #steam                            # Steam Game Launcher
    syncthing                        # File Synchronization
    waydroid                         # Android Emulator
  ];
}



