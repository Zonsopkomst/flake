{ config, pkgs, ... }:

{
  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  # Enable grub cryptodisk
  boot.loader.grub.enableCryptodisk=true;

  boot.initrd.luks.devices."luks-fd9d48bb-c085-40a7-bbea-d2e099d67cd8".keyFile = "/crypto_keyfile.bin";
  # Enable swap on luks
  boot.initrd.luks.devices."luks-8954ac79-16e8-4d4e-ae1c-3be9af26f4d0".device = "/dev/disk/by-uuid/8954ac79-16e8-4d4e-ae1c-3be9af26f4d0";
  boot.initrd.luks.devices."luks-8954ac79-16e8-4d4e-ae1c-3be9af26f4d0".keyFile = "/crypto_keyfile.bin";

  networking.hostName = "frija"; # Define your hostname.
  
  users.users.aspire = {
    isNormalUser = true;
    description = "aspire";
    extraGroups = [ "docker" "networkmanager" "wheel" ];
    packages = with pkgs; [
    ];
  };

  # Enable automatic login for the user.
  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "aspire";

  # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
  systemd.services."getty@tty1".enable = false;
  systemd.services."autovt@tty1".enable = false;

  system.stateVersion = "23.05"; # DO NOT EDIT

}
