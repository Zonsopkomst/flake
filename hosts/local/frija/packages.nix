{ config, pkgs, ... }:

{
  imports = [ ../../../common/fonts.nix];
      
  environment.systemPackages = with pkgs; [
    adw-gtk3                # GTK3 libadwaita Theme ported to GTK-3
    bleachbit               # PC Cleaner
    cpu-x                   # PC Information
    dconf                   # Gnome Configuration
    ntfs3g                  # FUSE Client
    p7zip                   # File Archiver
    papirus-icon-theme      # Icons
    phinger-cursors         # Cursor Theme
    powertop                # Power Manager
  ];
}