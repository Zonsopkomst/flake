{ config, pkgs, ... }:

{
  imports = [
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  boot.initrd.luks.devices."luks-57e53b6e-eb27-4bdc-961e-069ecf677d6b".device = "/dev/disk/by-uuid/57e53b6e-eb27-4bdc-961e-069ecf677d6b";
  boot.initrd.luks.devices."luks-57e53b6e-eb27-4bdc-961e-069ecf677d6b".keyFile = "/crypto_keyfile.bin";

  networking.hostName = "tyr";

  users.users.leeuwarden = {
    isNormalUser = true;
    description = "leeuwarden";
    extraGroups = [ "docker" "networkmanager" "wheel" ];
    packages = with pkgs; [
    ];
  };

  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "leeuwarden";

  # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
  # Discourse: https://discourse.nixos.org/t/unlocking-gnome-keyring-automatically-upon-login-with-kde-sddm/6966
  # Still not fixed: https://github.com/NixOS/nixpkgs/issues/86884#issuecomment-1134787613
  systemd.services."getty@tty1".enable = false;
  systemd.services."autovt@tty1".enable = false;

  environment.systemPackages = with pkgs; [ ];

  system.stateVersion = "22.05";
}
