{ config, pkgs, ... }:

{
  imports = [ ../../../common/fonts.nix];
    
  environment.systemPackages = with pkgs;
  [
    adw-gtk3                  # GTK3 libadwaita Theme ported to GTK-3
    bleachbit                 # PC Cleaner
    cpu-x                     # PC Information
    dconf                     # Gnome Configuration
    gamemode                  # Optimse Linux System Performance
    gcc                       # GNU Compiler Collection
    gramps                    # Genealogy Software     
    gsmartcontrol             # Disk Health Inspection Tool
    i2c-tools                 # Network Utilities
    inetutils                 # Network Utilities
    nox                       # Nix Tools
    ntfs3g                    # FUSE Client
    papirus-icon-theme        # Icons
    phinger-cursors           # Cursor Theme
    powertop                  # Power Manager
    protonup                  # Proton Installer
    steam                     # Steam Game Launcher
    strawberry                # Media Player
    syncthing                 # File Synchronization
  ];
}



