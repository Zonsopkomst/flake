# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.grub.enable = true;
  boot.loader.grub.device = "/dev/sda";
  boot.loader.grub.useOSProber = true;

  networking.hostName = "thuner"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Configure keymap in X11
  services.xserver = {
    layout = "us";
    xkbVariant = "";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.thuner = {
    isNormalUser = true;
    description = "thuner";
    extraGroups = [ "networkmanager" "wheel" "docker"];
    packages = with pkgs; [
      ];
  };


  # Services

  # Docker 
    # TODO Test: `docker ps` 
    # TODO Test: `docker run hello-world`
  virtualisation.docker.enable = true;

  # Cockpit
  # TODO Test: `http://<IP>:9090/`
  services.cockpit = {
    enable = true;
    port = 9090;
    settings = {
      WebService = {
        AllowUnencrypted = true;
      };
    };
  };

  # Portainer
  # TODO Test: `https://localhost:9443`
  virtualisation.oci-containers = {
    enable = true;
    containers.portainer = {
      image = pkgs.dockerTools.buildImage {
        name = "portainer/portainer:latest";
        tag = "latest";
      };
      ports = [ 9000:9000 ];
      volumes = [ "/var/lib/portainer:/var/lib/portainer" ];
    };
  };

  systemd.services.portainer = {
    description = "Portainer";
    serviceType = "forked";
    user = "portainer";
    group = "portainer";
    environment = {
      PORTAINER_DATA_DIR = "/var/lib/portainer";
    };
    execStart = "${pkgs.docker}/bin/docker start -a portainer";
    restart = "always";
  };


  # FreshRSS
  # services.freshrss = {
  #   enable = true;
  #   dataDir = "home/thuner/freshrss/data";
  #   user = "thuner";
  # };

  # Jellyfin
  #services.jellyfin = {
  #  enable = true;
  #  dataDir = "/home/thuner/jellyfin/data";
  #  user = "thuner";
  #  openFirewall = true;
  # };


  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    cockpit 
    #docker    # Container Manager
    #freshrss  # RSS Aggregator
    #jellyfin
    #jellyfin-web
    #jellyfin-ffmpeg
    wget
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # Open ports in the firewall.
  networking.firewall.allowedTCPPorts = [ 22 9000 9090 ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "23.05"; # Did you read the comment?

}