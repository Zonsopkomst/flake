{ config, pkgs, ... }:

{
  imports = [
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "tuisto";
  
  users.users.admin = {
    isNormalUser = true;
    description = "Admin";
    extraGroups = [ "docker" "lp" "networkmanager" "scanner" "wheel" ];
    packages = with pkgs; [
    ];
  };

  #warning: The option `services.xserver.displayManager.autoLogin' defined in `/nix/store/s9zj9b56sxx3wh27180czbbaqidvmfvb-source/hosts/remote/tuisto/default.nix' has been renamed to `services.displayManager.autoLogin'.

  #services.xserver.displayManager.autoLogin.enable = true;
  #services.xserver.displayManager.autoLogin.user = "admin";

  services.displayManager.autoLogin.enable = true;
  services.displayManager.autoLogin.user = "admin";

  # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
  systemd.services."getty@tty1".enable = false;
  systemd.services."autovt@tty1".enable = false;

  environment.systemPackages = with pkgs; [
  ];

  system.stateVersion = "22.11";

}
