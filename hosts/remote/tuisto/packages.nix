{ config, pkgs, ... }:

{
  imports = [ ../../../common/fonts.nix];
      
  environment.systemPackages = with pkgs; [
    adw-gtk3                  # GTK3 libadwaita Theme ported to GTK-3
    cpu-x                     # PC Information
    dconf                     # Gnome Configuration
    gamemode                  # Optimse Linux System Performance
    gsmartcontrol             # Disk Health Inspection Tool
    hplip                     # HP Print Drivers
    i2c-tools                 # Network Utilities
    inetutils                 # Network Utilities
    ntfs3g                    # FUSE Client
    p7zip                     # File Archiver
    papirus-icon-theme        # Icons
    phinger-cursors           # Cursor Theme
    powertop                  # Power Manager
    protonup                  # Proton Installer
    steam                     # Steam Game Launcher
    steam-run-native          # TODO steam-run (nixos native); can also try the following (https://linuxhint.com/how-to-instal-steam-on-nixos/ also see https://nixos.wiki/wiki/Steam):
    #(steam.override { extraPkgs = pkgs: [ mono gtk3 gtk3-x11 libgdiplus zlib ];
    #  nativeOnly = true; }).run
    #(steam.override { withPrimus = true; extraPkgs = pkgs: [ bumblebee glxinfo ];
    #  nativeOnly = true; }).run
    #(steam.override { withJava = true; })
    syncthing                 # File Synchronization
    #waydroid                  # Android Emulator
  ];
}



