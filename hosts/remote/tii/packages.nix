{ config, pkgs, ... }:

{
  imports = [ ../../../common/fonts.nix];
      
  environment.systemPackages = with pkgs; [
    adw-gtk3                  # GTK3 libadwaita Theme ported to GTK-3
    cpu-x                     # PC Information
    dconf                     # Gnome Configuration
    gsmartcontrol             # Disk Health Inspection Tool
    hplip                     # HP Print Drivers
    i2c-tools                 # Network Utilities
    inetutils                 # Network Utilities
    ntfs3g                    # FUSE Client
    p7zip                     # File Archiver
    papirus-icon-theme        # Icons
    phinger-cursors           # Cursor Theme
    powertop                  # Power Manager
    protonup                  # Proton Installer
    syncthing                 # File Synchronization
    #waydroid                  # Android Emulator
  ];
}



