{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "tii"; # Define your hostname.

  users.users.coleman = {
    isNormalUser = true;
    description = "coleman";
    extraGroups = [ "docker" "networkmanager" "wheel" ];
    packages = with pkgs; [
    ];
  };
 
  system.stateVersion = "23.05"; # DO NOT CHANGE

}
